package cn.ccccltd.waf.message.queue.db.config;

import javax.jms.ConnectionFactory;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;

import cn.ccccltd.waf.message.queue.db.constants.UpdateTableConstants;

/**
 * 创建日期:2017年11月13日
 * Title: 更新数据库的队列配置信息
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Configuration
public class UpdateTableQueueConfig {

	@Autowired
	private ConnectionFactory connectionFactory;
	
	/**
	 * 功能: 默认错误队列<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean(name = "updateTableQueue")
	public ActiveMQQueue updateTableQueue() {
		return new ActiveMQQueue(UpdateTableConstants.UPDATETABLE_QUEUE);
		
	}
	
	/**
	 * 功能: 错误队列模板管理<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean(name = "updateTableJmsTemplate")
	public JmsTemplate updateTableJmsTemplate() {
		
		JmsTemplate jmsTemplate = new JmsTemplate();
		
		jmsTemplate.setConnectionFactory(connectionFactory);
		jmsTemplate.setDefaultDestinationName(UpdateTableConstants.UPDATETABLE_QUEUE);
		
		return jmsTemplate;
		
	}
	
}
