package cn.ccccltd.waf.message.web.service;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;

/**
 * 创建日期:2017年11月1日
 * Title:消息接收后的逻辑接口类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public interface ForwardProcess {

	/**
	 * 功能: 转发内容<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月1日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param content
	 * @return
	 */
	public Object forwardContent(JSONObject content);
	
	/**
	 * 功能: 取消定时发送消息<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月6日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param commonId
	 * @return
	 */
	public Object deleteSchedulerMessage(String commonId);
	
	/**
	 * 功能: 查询消息的接口<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月13日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	public Object queryMessage(Map<String,String> params);
	
	/**
	 * 功能: 查询公用表字段<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月14日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	public Object queryCommon(Map<String,String> params);
	
	
	/**
	 * 功能: 统计消息数据<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月11日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	public Object queryStatMessage(Map<String,String> params);
	
	
	/**
	 * 功能: 根据系统名id统计查询<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月20日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	public Object queryStatMessageBySystemid(Map<String,String> params);
	
	/**
	 * 功能: 查询整个系统的消息<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月22日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	public Object queryStatMessageByCenter(Map<String,String> params);
	
	
	/**
	 * 功能: 根据业务模块统计查询<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月20日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	public Object queryStatMessageByModule(Map<String,String> params);
	
	
	/**
	 * 功能: 查询系统模块字典项<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月20日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	public Object querySystemidItem(Map<String,String> params);
	
	
	/**
	 * 功能: 查询模块项字典<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月20日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	public Object queryModuleItem(Map<String,String> params);
	
	
	/**
	 * 功能: 日志查询<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月21日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	public Object queryLog(Map<String, String> params);
}
