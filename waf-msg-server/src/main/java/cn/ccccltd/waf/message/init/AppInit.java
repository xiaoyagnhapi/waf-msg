package cn.ccccltd.waf.message.init;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import cn.ccccltd.waf.message.constant.MessageConstants;
import cn.ccccltd.waf.message.model.vo.Authority;
import cn.ccccltd.waf.message.util.sql.SqlTools;

/**
 * 创建日期:2017年10月31日
 * Title:初始化类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@WebServlet(name="appInit", loadOnStartup=1, urlPatterns = "/")
public class AppInit extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3830248270944148547L;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	/**用户缓存*/
	public static Map<String,Object> userMap = new HashMap<>();
	
	/**
	 * 功能:初始换将数据库权限放到缓存中
	 * 作者: yangjingjiang
	 * 创建日期:2017年10月31日
	 * 修改者: mender
	 * 修改日期: modifydate
	 * @throws ServletException
	 * @see javax.servlet.GenericServlet#init()
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void init() throws ServletException {
		
		super.init();
		
		//查询数据
		String select = SqlTools.getSelectAllProperty("", Authority.class);
		select  = select + " AND deleted <> '1'" ;
		List<Authority> query = jdbcTemplate.query(select, new BeanPropertyRowMapper(Authority.class));
		for (Authority wafMsgAuthority : query) {
			userMap.put(wafMsgAuthority.getSendSystemid(), wafMsgAuthority);
		}
		MessageConstants.INIT_DATA = false;
	}

	
	
}
