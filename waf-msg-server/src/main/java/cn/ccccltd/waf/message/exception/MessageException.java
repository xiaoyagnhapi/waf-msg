package cn.ccccltd.waf.message.exception;


/**
 * 创建日期:2017年11月15日
 * Title: 消息异常处理类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class MessageException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6531703464581013901L;
	/**成功或失败*/
	private String success = "false";
	/**返回的状态码*/
	private String stateCode = "500";
	/**返回的提示信息*/
	private String msg;
	public MessageException() {
		super();
	}
	public MessageException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
	public MessageException(String message, Throwable cause) {
		super(message, cause);
	}
	public MessageException(String message) {
		super(message);
		this.msg = message;
	}
	public MessageException(Throwable cause) {
		super(cause);
	}
	public MessageException(String success, String stateCode, String msg) {
		super(msg);
		this.success = success;
		this.stateCode = stateCode;
		this.msg = msg;
	}
	public MessageException(String stateCode, String msg) {
		super(msg);
		this.stateCode = stateCode;
		this.msg = msg;
	}
	/**
	 * @return 返回 success。
	
	 */
	public String getSuccess() {
		return success;
	}
	/**
	 * @param success 要设置的 success。
	
	 */
	public void setSuccess(String success) {
		this.success = success;
	}
	/**
	 * @return 返回 stateCode。
	
	 */
	public String getStateCode() {
		return stateCode;
	}
	/**
	 * @param stateCode 要设置的 stateCode。
	
	 */
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	/**
	 * @return 返回 msg。
	
	 */
	public String getMsg() {
		return msg;
	}
	/**
	 * @param msg 要设置的 msg。
	
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	/**
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月15日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MessageException [success=" + success + ", stateCode=" + stateCode + ", msg=" + msg + "]";
	}
	
	
}
