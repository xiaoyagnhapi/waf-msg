package cn.ccccltd.waf.message.base.consumer;

import javax.jms.Message;

/**
 * 创建日期:2017年10月30日
 * Title:消费者需要继承的接口
 * Description：消费者接口统一化
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public interface ConsumerService {

	/**
	 * 功能: 消费消息<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月3日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param message
	 */
	public void receiveMessage(Message message);
	
}
