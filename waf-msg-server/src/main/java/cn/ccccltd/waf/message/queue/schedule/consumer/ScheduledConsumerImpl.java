package cn.ccccltd.waf.message.queue.schedule.consumer;

import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

import cn.ccccltd.waf.message.base.consumer.ConsumerService;
import cn.ccccltd.waf.message.constant.MessageConstants;
import cn.ccccltd.waf.message.queue.schedule.constant.ScheduledConstants;
import cn.ccccltd.waf.message.util.JsonUtils;
import cn.ccccltd.waf.message.util.MessageUtils;

/**
 * 创建日期:2017年11月13日
 * Title: 定时器的消费者
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Component("scheduledConsumer")
public class ScheduledConsumerImpl  implements ConsumerService{

	private static Logger log = LoggerFactory.getLogger(ScheduledConsumerImpl.class);
	
	@Resource(name="scheduledJmsTemplate")
	private JmsTemplate scheduledJmsTemplate;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	/**
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月6日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param message
	 * @see cn.ccccltd.waf.message.base.consumer.ConsumerService#receiveMessage(javax.jms.Message)
	 */
	@Override
	@JmsListener(destination=ScheduledConstants.SCHEDULED_QUEUE, containerFactory="jmsListenerContainerFactory" )
	public void receiveMessage(Message message) {
		
		String messageText = MessageUtils.getMessageText(message);
		
		JSONObject jo = JsonUtils.toObject(messageText, JSONObject.class);
		
		//消息类型
		String category = (String)jo.get("category");
		//消息id
		String messageId = (String)jo.get(category+"Id");
		
		StringBuilder sb = new StringBuilder(500);
		
		sb.append(" UPDATE waf_msg_message_").append(category);
		sb.append(" SET send_state = '").append(MessageConstants.SEND_FAIL_CANCEL).append("'");
		sb.append(" WHERE ").append(category).append("_id = '").append(messageId).append("'");
		
		try {
			
			jdbcTemplate.update(sb.toString());
			log.debug(MessageConstants.LOG_DEBUG_PREFIX + sb.toString());
		} catch (DataAccessException e) {
			
			//更新数据库失败重新更新数据库
			scheduledJmsTemplate.send(new MessageCreator() {

				@Override
				public Message createMessage(Session session) throws JMSException {
					TextMessage message = session.createTextMessage(JsonUtils.toJson(jo));
					return message;
				}
				
			});
			
			e.printStackTrace();
		}
		
	}
	
}
