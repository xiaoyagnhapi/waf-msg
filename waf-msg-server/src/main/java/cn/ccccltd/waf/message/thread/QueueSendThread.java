package cn.ccccltd.waf.message.thread;

import java.util.List;
import java.util.concurrent.Callable;

import org.springframework.jms.core.JmsTemplate;

import com.alibaba.fastjson.JSONObject;

import cn.ccccltd.waf.message.model.MessageCommon;
import cn.ccccltd.waf.message.util.JsonUtils;
import cn.ccccltd.waf.message.util.QueueUtils;

/**
 * 创建日期:2017年11月13日
 * Title: 消息发送线程类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 * @param <T>
 */
public class QueueSendThread<T extends MessageCommon> implements Callable<Integer> {

	
	private List<T> msg;
	
	private JmsTemplate jmsTemplate;
	
	private int count;

	public QueueSendThread(List<T> msg, JmsTemplate jmsTemplate) {
		super();
		this.msg = msg;
		this.jmsTemplate = jmsTemplate;
	}


	/**
	 * 功能: 具体的消息发送<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月21日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 * @throws Exception
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Integer call() throws Exception {
		
		for (T t : msg) {
			QueueUtils.sendMessageToQueue(jmsTemplate, JsonUtils.toObject(JsonUtils.toJson(t), JSONObject.class));
			count++;
		}
		
		return count;
	}

	
	
}
