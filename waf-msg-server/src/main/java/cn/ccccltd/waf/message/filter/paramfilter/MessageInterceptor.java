package cn.ccccltd.waf.message.filter.paramfilter;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cn.ccccltd.waf.message.constant.MessageConstants;
import cn.ccccltd.waf.message.model.params.QueryParams;
import cn.ccccltd.waf.message.model.vo.Authority;
import cn.ccccltd.waf.message.thread.threadlocal.MessageContext;
import cn.ccccltd.waf.message.util.JsonUtils;
import cn.ccccltd.waf.message.util.MessageUtils;

/**
 * 创建日期:2017年11月21日
 * Title:消息的拦截器
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class MessageInterceptor extends HandlerInterceptorAdapter {

	private static Logger log = LoggerFactory.getLogger(MessageInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		String requestURI = request.getRequestURI();
		
		log.info(MessageConstants.LOG_DEBUG_PREFIX + "请求url" + requestURI);
		
		//数据过滤初始化，默认初始化为过滤
		MessageContext.setValue(MessageConstants.NOTFILTERDATA, MessageConstants.FALSE);
		MessageContext.setValue(MessageConstants.ISFILTERDATA, MessageConstants.FALSE);
		
		// 获取url参数中json字段
		String parameter = request.getParameter(MessageConstants.JSON);
		
		log.info(MessageConstants.LOG_DEBUG_PREFIX + "请求参数" + parameter);
		
		QueryParams queryParams = new QueryParams();

		if (null != parameter) {
			queryParams = JsonUtils.toObject(parameter, QueryParams.class);
		}

		// 分页处理
		Map<String, Integer> page = queryParams.getPage();
		// 一页数据值
		Integer rpp = 10;
		// 页号
		Integer pno = 0;
		if (null != page && page.size() > 0) {
			rpp = page.get(MessageConstants.RPP);
			pno = page.get(MessageConstants.PNO);

		}
		MessageContext.setValue(MessageConstants.RPP, rpp);
		MessageContext.setValue(MessageConstants.PNO, pno);
		
		// 是否是管理员
		Authority wafMsgAuthority = (Authority) MessageContext.getValue(MessageConstants.SESSION);
		
		if (!MessageUtils.isAdmin(wafMsgAuthority)) {
			log.info("不是管理员+++" + MessageConstants.LOG_DEBUG_PREFIX + wafMsgAuthority.getSendSystemid());
			// 权限过滤是否开启
			MessageContext.setValue(MessageConstants.ISFILTERDATA, MessageConstants.TRUE);
			MessageContext.setValue(MessageConstants.SENDSYSTEMID, wafMsgAuthority.getSendSystemid());
			
		}
		// 消息类型
		MessageContext.setValue(MessageConstants.CATEGORY, queryParams.getCategory());
		// 查询条件
		MessageContext.setValue(MessageConstants.QUERYITEMS, queryParams.getQueryItems());
		// 分页条件
		MessageContext.setValue(MessageConstants.ORDERS, queryParams.getOrders());

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

	}

}
