package cn.ccccltd.waf.message.filter.sqlfilter.impl;

import org.springframework.stereotype.Component;

import cn.ccccltd.waf.message.constant.MessageConstants;
import cn.ccccltd.waf.message.filter.sqlfilter.DBAdaptation;
import cn.ccccltd.waf.message.thread.threadlocal.MessageContext;

/**
 * 创建日期:2017年11月15日
 * Title: mysql分页
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Component("mysqlAdapter")
public class MysqlAdapter implements DBAdaptation {

	@Override
	public String getLimitSql(String sql) {

		StringBuilder sb = new StringBuilder(sql);

		int pageSize = Integer.valueOf(MessageContext.getValue(MessageConstants.RPP)+"");
		int pageNo = Integer.valueOf(MessageContext.getValue(MessageConstants.PNO)+"")-1;
		if(pageNo<0) {
			pageNo=0;
		}
		
		//拼接分页
		return sb.append(" LIMIT ").append(pageNo * pageSize).append(", ").append(pageSize).toString();
	
	}


	
}
