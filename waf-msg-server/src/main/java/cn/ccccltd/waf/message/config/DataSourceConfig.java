package cn.ccccltd.waf.message.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.core.JdbcTemplate;

import com.alibaba.druid.filter.Filter;
import com.alibaba.druid.pool.DruidDataSource;

import cn.ccccltd.waf.message.filter.sqlfilter.FormatSqlFilter;
import cn.ccccltd.waf.message.init.PropertyContext;

/**
 * 创建日期:2017年10月31日
 * Title:数据源配置
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Configuration
@Order(-2)
public class DataSourceConfig {

	/**
	 * 功能: 数据库核心操作类<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月21日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean
	public JdbcTemplate jdbcTemplate() {
		
		JdbcTemplate jt = new JdbcTemplate();
		jt.setDataSource(dataSource());
		
		return jt;
		
	}
	
	/**
	 * 功能: 阿里数据源<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月21日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean(initMethod = "init", destroyMethod = "clone")
	public DruidDataSource  dataSource() {
		
		DruidDataSource dataSource = new DruidDataSource();
		
		dataSource.setDriverClassName(PropertyContext.getProperty("db.driver"));
		dataSource.setUrl(PropertyContext.getProperty("db.url"));
		dataSource.setUsername(PropertyContext.getProperty("db.username"));
		dataSource.setPassword(PropertyContext.getProperty("db.password"));
		//初始化时建立物理连接的个数，缺省值为0
		dataSource.setInitialSize(Integer.valueOf(PropertyContext.getProperty("db.initialSize")));
		//最小连接池数量
		dataSource.setMinIdle(Integer.valueOf(PropertyContext.getProperty("db.minIdle")));
		//最大连接池数量，缺省值为8
		dataSource.setMaxActive(Integer.valueOf(PropertyContext.getProperty("db.maxActive")));
		/**
		 * 获取连接时最大等待时间，单位毫秒。配置了maxWait之后，缺省启用公平锁，
		 * 并发效率会有所下降，如果需要可以通过配置useUnfairLock属性为true使用非公平锁。
		 */
		dataSource.setMaxWait(Integer.valueOf(PropertyContext.getProperty("db.maxWait")));
		List<Filter> filters = new ArrayList<>();
		filters.add(new FormatSqlFilter());
		dataSource.setProxyFilters(filters);
		
		return dataSource;
	}
	
}
