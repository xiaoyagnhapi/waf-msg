package cn.ccccltd.waf.message.queue.log.util;

import java.util.HashMap;
import java.util.Map;

import cn.ccccltd.waf.message.queue.log.constant.LogConstants;

/**
 * 创建日期:2017年11月4日
 * Title: 封装日志消息的属性
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class LogMessageUtils {

	/**
	 * 功能: 将日志消息的特有属性放到message属性中<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月4日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param messageId 消息id
	 * @param remoteCode 第三方返回的状态码
	 * @param remoteMsg 第三方返回的消息
	 * @return
	 */
	public static Map<String,String> getSendLogMap(String messageId, String remoteCode, String remoteMsg){
		
		Map<String,String> map = new HashMap<>(3);
		
		map.put(LogConstants.LOG_MESSAGE_ID, messageId);
		map.put(LogConstants.LOG_REMOTE_CODE, remoteCode);
		map.put(LogConstants.LOG_REMOTE_MSG, remoteMsg);
		
		return map;
		
	}
}
