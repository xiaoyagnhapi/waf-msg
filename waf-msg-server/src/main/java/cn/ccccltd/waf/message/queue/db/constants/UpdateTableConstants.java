package cn.ccccltd.waf.message.queue.db.constants;

/**
 * 创建日期:2017年11月2日
 * Title: 
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class UpdateTableConstants {
	
	/**错误队列的名称*/
	public static final String UPDATETABLE_QUEUE = "waf.message.queue.updatetable";
	
}
