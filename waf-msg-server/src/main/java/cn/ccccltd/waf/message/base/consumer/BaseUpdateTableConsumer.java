package cn.ccccltd.waf.message.base.consumer;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

import cn.ccccltd.waf.message.constant.MessageConstants;
import cn.ccccltd.waf.message.util.JsonUtils;
import cn.ccccltd.waf.message.util.MessageUtils;
import cn.ccccltd.waf.message.util.QueueUtils;
import cn.ccccltd.waf.message.util.sql.SqlContext;

/**
 * 创建日期:2017年11月13日
 * Title: 更新表消费者公用处理
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Component
public abstract class BaseUpdateTableConsumer {

	private static Logger log = LoggerFactory.getLogger(BaseUpdateTableConsumer.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Resource(name="errorJmsTemplate")
	private JmsTemplate jmsTemplate;
	
	/**
	 * 功能: 获取更新状态sql上下文<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月13日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param smsMessage
	 * @return
	 */
	protected abstract SqlContext getUpdateSqlContext(JSONObject smsMessage);
	
	/**
	 * 功能: 获取更新状态的JmsTemplate<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月13日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	protected abstract JmsTemplate getUpdateTableJmsTemplate();
	
	/**
	 * 功能: 消费消息<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月3日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param message
	 */
	public void receiveMessage(Message message) {
		
		//初始化更新条数
		int updateCount = 0;
		String msg = MessageUtils.getMessageText(message);
		
		String statue = MessageUtils.getMessageStringProperty(message, MessageConstants.SEND_STATUE_PROPERTY);
		
		JSONObject smsMessage = JsonUtils.toObject(msg, JSONObject.class);
		
		//获取更新sql
		SqlContext sqlContext = getUpdateSqlContext(smsMessage);
		String sql = sqlContext.getSql();
		
		try {
			updateCount = jdbcTemplate.update(sql, sqlContext.getParams());
		} catch (DataAccessException e) {
			Map<String, String> param = new HashMap<>(1);
			param.put(MessageConstants.SEND_STATUE_PROPERTY, statue);
			//发送消息到更新状态队列
			QueueUtils.sendMessageToQueue(getUpdateTableJmsTemplate(), smsMessage, param);
		}
		
		//发送判断是发送重试还是发送失败，发送失败之后不进行重发处理
		if (!MessageConstants.SEND_SUCCESS.equals(statue) && updateCount > 0 && !MessageConstants.SEND_FAIL.equals(smsMessage.getString(MessageConstants.SENDSTATE)) ){
			
			Map<String, String> param = new HashMap<>(1);
			
			param.put(MessageConstants.SELECTOR, smsMessage.getString(MessageConstants.CATEGORY) + MessageConstants.ERROR_MESSAGE_SELECTOR);
			//发送失败的消息到发送错误队列重试
			QueueUtils.sendMessageToQueue(jmsTemplate, smsMessage, param);
			
		}
		
		log.debug(MessageConstants.LOG_DEBUG_PREFIX + "更新waf_msg_message_sms数据准条数" 
				+ updateCount + MessageConstants.LOG_DEBUG_PREFIX + JsonUtils.toJson(smsMessage));
		
	}
}
