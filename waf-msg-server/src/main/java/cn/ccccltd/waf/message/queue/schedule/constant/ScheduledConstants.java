package cn.ccccltd.waf.message.queue.schedule.constant;

/**
 * 创建日期:2017年11月6日
 * Title: 定时任务常量
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class ScheduledConstants {

	/**定时取消处理队列*/
	public static final String SCHEDULED_QUEUE = "waf.message.queue.scheduled.cacel";
	
	
}
