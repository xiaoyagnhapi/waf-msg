package cn.ccccltd.waf.message.util.sql;

/**
 * 创建日期:2017年11月4日
 * Title: sql上下文
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class SqlContext {

	/**sql语句*/
	private StringBuilder sql;
	/**sql中?对应的参数*/
	private Object[] params;
	
	public SqlContext() {
	}

	public SqlContext(StringBuilder sql) {
		this.sql = sql;
	}

	public SqlContext(StringBuilder sql, Object[] params) {
		this.sql = sql;
		this.params = params;
	}

	public String getSql() {
		return sql.toString();
	}

	public StringBuilder getSqlBuilder() {
		return sql;
	}

	public void setSql(StringBuilder sql) {
		this.sql = sql;
	}

	public void setSql(String sql) {
		this.sql = new StringBuilder(sql);
	}

	public Object[] getParams() {
		return params;
	}

	public void setParams(Object[] params) {
		this.params = params;
	}

}