package cn.ccccltd.waf.message.queue.error.config;

import javax.jms.ConnectionFactory;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;

import cn.ccccltd.waf.message.queue.error.constant.ErrorConstants;

/**
 * 创建日期:2017年11月2日
 * Title: 错误队列的配置
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Configuration
public class ErrorQueueConfig {

	@Autowired
	private ConnectionFactory connectionFactory;
	
	/**
	 * 功能: 默认错误队列<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean(name = "errorQueue")
	public ActiveMQQueue errorQueue() {
		return new ActiveMQQueue(ErrorConstants.ERROR_QUEUE);
		
	}
	
	/**
	 * 功能: 错误队列模板管理<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean(name = "errorJmsTemplate")
	public JmsTemplate errorJmsTemplate() {
		
		JmsTemplate jmsTemplate = new JmsTemplate();
		
		jmsTemplate.setConnectionFactory(connectionFactory);
		jmsTemplate.setDefaultDestinationName(ErrorConstants.ERROR_QUEUE);
		
		return jmsTemplate;
		
	}
	
}
