package cn.ccccltd.waf.message.web.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.alibaba.fastjson.JSONObject;

import cn.ccccltd.waf.message.base.common.SeachData;
import cn.ccccltd.waf.message.base.provider.BaseMessageService;
import cn.ccccltd.waf.message.constant.MessageConstants;
import cn.ccccltd.waf.message.context.SpringContext;
import cn.ccccltd.waf.message.model.result.MessageResult;
import cn.ccccltd.waf.message.thread.threadlocal.MessageContext;
import cn.ccccltd.waf.message.util.QueueUtils;
import cn.ccccltd.waf.message.web.service.ForwardProcess;


/**
 * 创建日期:2017年11月13日
 * Title: 业务分发处理类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Component("forwardProcess")
public class ForwardProcessImpl implements ForwardProcess {

	private static Logger log = LoggerFactory.getLogger(ForwardProcessImpl.class);
	
	@Resource(name = "seachData")
	SeachData seachData;
	
	/**
	 * 功能: 获取消息实体<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月10日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param content
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private BaseMessageService getProviderService(JSONObject... content) {
		String categoryBean = (String)MessageContext.getValue(MessageConstants.CATEGORY);
		if (StringUtils.isBlank(categoryBean)) {
			Assert.isTrue(content.length>0,"消息类型不能为空！");
			categoryBean = (String)content[0].get(MessageConstants.CATEGORY);
		}
		
		Assert.hasText(categoryBean,"消息类型不能为空！");
		
		//从spring容器中取出对应的实现类
		return SpringContext.getBean(categoryBean);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Object forwardContent(JSONObject content) {
		
		log.debug(MessageConstants.LOG_DEBUG_PREFIX + "访问转发service成功！");
		
		//转到各个消息发送处理类,并返回处理信息
		MessageResult returnMessage = (MessageResult)getProviderService(content).sendMessage(content);
		
		return returnMessage;
	}

	@Override
	public Object deleteSchedulerMessage(String commonId) {

		Assert.hasText(commonId,"定时任务的组id不能为空！");
		
		QueueUtils.deleteSchedulerJob(commonId);
		
		return new MessageResult("true", "200", "删除定时任务成功!");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Object queryMessage(Map<String,String> params) {
		
		BaseMessageService providerService = getProviderService();
		Object queryMessage = providerService.queryMessage(params);
		
		return queryMessage;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Object queryStatMessage(Map<String,String> params) {
		BaseMessageService providerService = getProviderService();
		Object queryMessage = providerService.queryStatMessage(params);
		return queryMessage;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object queryStatMessageBySystemid(Map<String, String> params) {
		
		Object queryStatMessageBySystemid = getProviderService().queryStatMessageBySystemid(params);
		return queryStatMessageBySystemid;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object queryStatMessageByModule(Map<String, String> params) {
		
		Object queryStatMessageByModule = getProviderService().queryStatMessageByModule(params);
		return queryStatMessageByModule;
		
	}

	@Override
	public Object queryStatMessageByCenter(Map<String, String> params) {
		
		Object queryStatMessageByCenter = seachData.queryStatMessageByCenter(params);
		return queryStatMessageByCenter;
		
	}
	
	@Override
	public Object queryCommon(Map<String,String> params) {
		
		Object queryMessage = seachData.queryCommon(params);
		
		return queryMessage;
	}
	
	@Override
	public Object querySystemidItem(Map<String, String> params) {
		
		Object querySystemidItem = seachData.querySystemidItem(params);
		return querySystemidItem;
		
	}

	@Override
	public Object queryModuleItem(Map<String, String> params) {
		
		Object queryModuleItem = seachData.queryModuleItem(params);
		return queryModuleItem;
		
	}
	
	@Override
	public Object queryLog(Map<String, String> params) {
		
		Object queryModuleItem = seachData.queryLog(params);
		return queryModuleItem;
		
	}

}
