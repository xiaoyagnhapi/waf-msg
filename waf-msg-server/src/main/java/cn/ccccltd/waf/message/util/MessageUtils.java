package cn.ccccltd.waf.message.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import cn.ccccltd.waf.message.constant.MessageConstants;
import cn.ccccltd.waf.message.model.vo.Authority;

/**
 * 创建日期:2017年11月2日
 * Title: jms工具类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class MessageUtils {

	/**
	 * 功能: 消息处理工具类<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param message
	 * @return
	 */
	public static String getMessageText(Message message) {
		
		String text = "";
		TextMessage msg = (TextMessage) message;
		try {
			text = msg.getText();
		} catch (JMSException e) {
			e.printStackTrace();
		}
		
		return text;
		
	}
	
	/**
	 * 功能: 获取property属性<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param message
	 * @param propertyName
	 * @return
	 */
	public static String getMessageStringProperty(Message message, String propertyName) {
		
		String property = "";
		try {
			property = message.getStringProperty(propertyName);
		} catch (JMSException e) {
			e.printStackTrace();
		}
		return property;
		
	}
	
	/**
	 * 输出JSON格式数据。
	 * <p>
	 * 把任意类型的数据对象转换成JSON格式字符串后，通过HTTP响应对象输出到请求端
	 * （响应内容的类型设置为"application/json;charset=utf-8"）
	 * 
	 * @param response
	 *            HTTP响应对象
	 * @param obj
	 *            待输出的数据对象
	 */

	public static void writeJsonToPage(HttpServletResponse response, Object responseObject) {

		//将实体对象转换为JSON Object转换  
	    String responseString = JsonUtils.toJson(responseObject);
	    response.setCharacterEncoding("UTF-8");  
	    response.setContentType("application/json; charset=utf-8");  
	    PrintWriter out = null;  
	    try {  
	        out = response.getWriter();  
	        out.append(responseString);  
	    } catch (IOException e) {  
	        e.printStackTrace();  
	    } finally {  
	        if (out != null) {  
	            out.close();  
	        }  
	    }  
	}
	
	/**
	 * 功能: 获取异常详细<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月16日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param e
	 * @return
	 */
	public static String getExceptionStackTrace(Exception e) {

		StringWriter sw = null;
		PrintWriter pw = null;

		try {
			sw = new StringWriter();
			pw = new PrintWriter(sw);

			// 将出错的栈信息输出到printWriter中
			e.printStackTrace(pw);

			pw.flush();
			sw.flush();

		} finally {
			if (sw != null) {
				try {
					sw.close();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
			if (pw != null) {
				pw.close();
			}
		}
		
		return (sw == null) ? "" : "<pre>" + sw.toString().replaceAll("\r\n", "<br>") + "</pre>";
	}
	
	/**
	 * 功能: 判断是否是管理员<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月21日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param wafMsgAuthority
	 * @return
	 */
	public static boolean isAdmin(Authority wafMsgAuthority) {
		
		return (StringUtils.isNotBlank(wafMsgAuthority.getIsAdmin())
				&& (MessageConstants.TRUE.equals(wafMsgAuthority.getIsAdmin())));
		
	}
}
