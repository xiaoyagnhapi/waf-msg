package cn.ccccltd.waf.message.filter.sqlfilter.impl;

import org.springframework.stereotype.Component;

import cn.ccccltd.waf.message.constant.MessageConstants;
import cn.ccccltd.waf.message.filter.sqlfilter.DBAdaptation;
import cn.ccccltd.waf.message.thread.threadlocal.MessageContext;

/**
 * 创建日期:2017年11月17日
 * Title: oracle的sql兼容处理
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Component("oracleAdapter")
public class OracleAdapter implements DBAdaptation{

	@Override
	public String getLimitSql(String sql) {

		StringBuilder sb = new StringBuilder();

		int pageSize = Integer.valueOf(MessageContext.getValue(MessageConstants.RPP)+"");
		int pageNo = Integer.valueOf(MessageContext.getValue(MessageConstants.PNO)+"");
		if(pageNo<1) {
			pageNo=1;
		}
		
		sb.append(" select ");
        sb.append("     * ");
        sb.append(" from ");
        sb.append("     ( ");
        sb.append("      select ");
        sb.append("          dumb_table.*, ");
		sb.append("          rownum dumb_col ");
		sb.append("      from ");
		sb.append("          ( ");
		
		// 被包装的业务系统sql
		sb.append(sql);

		sb.append("          ) dumb_table");
		sb.append("     ) ");
		sb.append(" where 0 = 0 ");
		sb.append("     and dumb_col <= ").append(pageNo * pageSize);
		
//		处理页码负数问题
		pageNo = pageNo - 1;
		if(pageNo<0) {
			pageNo=0;
		}
		
		sb.append("     and dumb_col > ").append(pageNo * pageSize);

		return sb.toString();
	}

}
