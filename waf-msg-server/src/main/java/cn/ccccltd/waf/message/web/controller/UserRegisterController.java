package cn.ccccltd.waf.message.web.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.ccccltd.waf.message.model.vo.Authority;
import cn.ccccltd.waf.message.web.service.UserRegister;

/**
 * 创建日期:2017年11月16日
 * Title: 用户注册的Controller
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@RestController
@RequestMapping("/message/regist")
public class UserRegisterController {

	@Autowired
	private UserRegister userRegister;
	
	
	/**
	 * 功能: 添加用户<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月16日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param wma
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public Object saveUser(@RequestBody Authority wma) {
		
		Object result = userRegister.saveUser(wma);
		return result;
		
	}
	
	/**
	 * 功能: 删除用户<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月15日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param wma
	 * @return
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	public Object deleteUser(@RequestParam String id) {
		
		Object result = userRegister.deleteUser(id);
		return result;
		
	}
	
	/**
	 * 功能: 修改用户信息<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月16日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param wma
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public Object editUser(@RequestBody Authority wma) {
		
		Object result = userRegister.editUser(wma);
		return result;
		
	}
	
	/**
	 * 功能: 查询单个用户信息<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月16日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param id
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public Object getUserById(@RequestParam String id) {
		
		Object result = userRegister.getUserById(id);
		return result;
		
	}
	
	/**
	 * 功能: 分页查询用户信息<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月16日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public Object getUserByPage(@RequestParam Map<String,Object> map) {
		
		Object result = userRegister.getUserByPage(map);
		return result;
		
	}
	
}
