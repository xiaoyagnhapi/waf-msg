package cn.ccccltd.waf.message.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import cn.ccccltd.waf.message.model.result.MessageResult;
import cn.ccccltd.waf.message.util.MessageUtils;

/**
 * 创建日期:2017年11月15日
 * Title: 异常处理类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@ControllerAdvice
public class MessageExceptionHandler {

	@ExceptionHandler(Exception.class)
	@ResponseBody
	public MessageResult messageError(final Exception e, final WebRequest req){
		
		//打印错误日志
		e.printStackTrace();
		MessageResult rs = new MessageResult("false", HttpStatus.INTERNAL_SERVER_ERROR.toString(), e.getMessage(), MessageUtils.getExceptionStackTrace(e));
		return rs;
		
	}
	
}
