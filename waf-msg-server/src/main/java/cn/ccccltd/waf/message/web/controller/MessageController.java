package cn.ccccltd.waf.message.web.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.ccccltd.waf.message.constant.MessageConstants;
import cn.ccccltd.waf.message.web.service.ForwardProcess;

/**
 * 创建日期:2017年11月1日
 * Title:消息的统一接收端
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@RestController
@RequestMapping("/message")
public class MessageController {

	protected static Logger log = LoggerFactory.getLogger(MessageController.class);
	
	@Autowired
	private ForwardProcess forwardProcess;
	
	/**
	 * 功能: 接收消息的接口<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月1日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param content
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public Object message(@RequestBody JSONObject content) {
		
		log.debug(MessageConstants.LOG_DEBUG_PREFIX+"消息传入到后台！！"+content);
		
		Object returnMessage  = forwardProcess.forwardContent(content);
		
		return returnMessage;
		
	}
	
	/**
	 * 功能: 删除定时消息<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月15日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param commonId
	 * @return
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	public Object deleteschedulerMessage(@RequestParam String commonId) {
		
		log.debug(MessageConstants.LOG_DEBUG_PREFIX+"删除任务传入后台！！"+commonId);
		
		Object returnMessage  = forwardProcess.deleteSchedulerMessage(commonId);
		
		return returnMessage;
		
	}
	
	/**
	 * 功能: 消息列表<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月15日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/msgList", method = RequestMethod.GET)
	public Object queryMessage(@RequestParam Map<String,String> params) {
		
		log.debug(MessageConstants.LOG_DEBUG_PREFIX+"msgList查询任务传入！！");
		
		Object returnMessage  = forwardProcess.queryMessage(params);
		
		return returnMessage;
		
	}
	
	/**
	 * 功能: 消息统计<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月15日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/msgStat", method = RequestMethod.GET)
	public Object queryStatMessage(@RequestParam Map<String,String> params) {
		
		Object queryMessage = forwardProcess.queryStatMessage(params);
		
		return queryMessage;
		
	}
	
	/**
	 * 功能: 整体消息信息<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月15日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/msgCommon", method = RequestMethod.GET)
	public Object queryCommon(@RequestParam Map<String,String> params) {
		
		Object queryMessage = forwardProcess.queryCommon(params);
		
		return queryMessage;
		
	}
	
	/**
	 * 功能: 根据系统名id统计查询<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月20日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/msgStatBySystemid", method = RequestMethod.GET)
	public Object queryStatMessageBySystemid(@RequestParam Map<String,String> params) {
		
		Object queryMessage = forwardProcess.queryStatMessageBySystemid(params);
		
		return queryMessage;
		
	}
	
	
	/**
	 * 功能: 根据业务模块统计查询<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月20日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/msgStatByModule", method = RequestMethod.GET)
	public Object queryStatMessageByModule(@RequestParam Map<String,String> params) {
		
		Object queryMessage = forwardProcess.queryStatMessageByModule(params);
		
		return queryMessage;
		
	}
	
	/**
	 * 功能: 查询消息中心的消息<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月22日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/msgStatByCenter", method = RequestMethod.GET)
	public Object queryStatMessageByCenter(@RequestParam Map<String,String> params) {
		
		Object queryMessage = forwardProcess.queryStatMessageByCenter(params);
		
		return queryMessage;
		
	}
	
	
	/**
	 * 功能: 查询系统模块字典项<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月20日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/systemidItem", method = RequestMethod.GET)
	public Object querySystemidItem(@RequestParam Map<String,String> params) {
		
		Object queryMessage = forwardProcess.querySystemidItem(params);
		
		return queryMessage;
		
	}
	
	
	/**
	 * 功能: 查询模块项字典<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月20日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/moduleItem", method = RequestMethod.GET)
	public Object queryModuleItem(@RequestParam Map<String,String> params) {
		
		Object queryMessage = forwardProcess.queryModuleItem(params);
		
		return queryMessage;
		
	}
	
	/**
	 * 功能: 查询模块项字典<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月20日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/log", method = RequestMethod.GET)
	public Object queryLog(@RequestParam Map<String,String> params) {
		
		Object queryMessage = forwardProcess.queryLog(params);
		
		return queryMessage;
		
	}
	
}
