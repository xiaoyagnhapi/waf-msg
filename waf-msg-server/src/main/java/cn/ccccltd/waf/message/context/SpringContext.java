package cn.ccccltd.waf.message.context;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 创建日期:2017年10月31日
 * Title: SpringContext的工具类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Component
public class SpringContext implements ApplicationContextAware{
	
	protected static Logger log = Logger.getLogger(SpringContext.class);

	private static ApplicationContext applicationContext;

	/**
	 * 功能: 从容器中拿出对应的类<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年10月31日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param beanName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getBean(String beanName) {
		
		if (applicationContext == null) {
			return null;
		}

		if (applicationContext != null 
				&& applicationContext.containsBean(beanName)) {
			
			Object bean = applicationContext.getBean(beanName);
			
			if (log.isDebugEnabled()) {
				log.debug("从spring容器中获取到名称为'" + beanName + "' 的类--" + (bean == null ? null : bean.getClass().getName()));
			}
			
			return (T) bean;
			
		} else {
			
			if (log.isDebugEnabled()) {
				log.error("'" + beanName + "'类未定义，或spring容器未初始化！");
			}
			
			return null;
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext ac) throws BeansException {
		applicationContext = ac;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

}
