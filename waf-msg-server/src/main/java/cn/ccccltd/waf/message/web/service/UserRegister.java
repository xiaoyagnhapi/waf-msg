package cn.ccccltd.waf.message.web.service;

import java.util.Map;

import cn.ccccltd.waf.message.model.vo.Authority;

/**
 * 创建日期:2017年11月15日
 * Title: 用户注册接口
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public interface UserRegister {

	/**
	 * 功能: 添加用户<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月15日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param wma
	 * @return
	 */
	public Object saveUser(Authority wma);
	
	/**
	 * 删除用户
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月17日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param id
	 * @return
	 */
	public Object deleteUser(String id);
	
	/**
	 * 功能: 修改用户信息<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月15日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param wma
	 * @return
	 */
	public Object editUser(Authority wma);
	
	/**
	 * 功能: 查询单个用户信息<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月15日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param id
	 * @return
	 */
	public Object getUserById(String id);
	
	/**
	 * 功能: 分页查询用户信息<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月15日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param map
	 * @return
	 */
	public Object getUserByPage(Map<String,Object> map);
	
}
