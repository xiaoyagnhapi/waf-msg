package cn.ccccltd.waf.message.thread.threadlocal;

import java.util.HashMap;
import java.util.Map;


/**
 * 创建日期:2017年11月7日
 * Title: 系统的参数上下文容器
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class MessageContext{

	/**本地线程参数变量*/
    private static final ThreadLocal<Map<Object, Object>> CXT = new ThreadLocal<Map<Object, Object>>() {  
        @Override  
        protected Map<Object, Object> initialValue() {  
            return new HashMap<Object, Object>();  
        }  
  
    };  

    /**
     * 功能: 根据key获取值 <br>
     * 作者: yangjingjiang <br>
     * 创建日期:2017年11月16日 <br>
     * 修改者: mender <br>
     * 修改日期: modifydate <br>
     * @param key
     * @return
     */
    public static Object getValue(Object key) {  
        if(CXT.get() == null) {  
            return null;  
        }  
        return CXT.get().get(key);  
    }  
  
    /**
     * 功能: 存储值<br>
     * 作者: yangjingjiang <br>
     * 创建日期:2017年11月16日 <br>
     * 修改者: mender <br>
     * 修改日期: modifydate <br>
     * @param key
     * @param value
     * @return
     */
    public static Object setValue(Object key, Object value) {  
        Map<Object, Object> cacheMap = CXT.get();  
        if(cacheMap == null) {  
            cacheMap = new HashMap<Object, Object>();  
            CXT.set(cacheMap);  
        }  
        return cacheMap.put(key, value);  
    }  
  
    /**
     * 功能: 根据key移除值 <br>
     * 作者: yangjingjiang <br>
     * 创建日期:2017年11月16日 <br>
     * 修改者: mender <br>
     * 修改日期: modifydate <br>
     * @param key
     */
    public static void removeValue(Object key) {  
        Map<Object, Object> cacheMap = CXT.get();  
        if(cacheMap != null) {  
            cacheMap.remove(key);  
        }  
    }  
  
    /**
     * 功能: 重置 <br>
     * 作者: yangjingjiang <br>
     * 创建日期:2017年11月16日 <br>
     * 修改者: mender <br>
     * 修改日期: modifydate <br>
     */
    public static void reset() {  
        if(CXT.get() != null) {  
            CXT.get().clear();  
        }  
    }  
    
    /**
     * 功能: 清楚本地线程,需要在调用之后清理，线程多次请求可能会请求到相同的<br>
     * 作者: yangjingjiang <br>
     * 创建日期:2017年11月18日 <br>
     * 修改者: mender <br>
     * 修改日期: modifydate <br>
     */
    public static void remove() {  
    	if(CXT != null) {  
    		CXT.remove();  
    	}  
    }  
	
}
