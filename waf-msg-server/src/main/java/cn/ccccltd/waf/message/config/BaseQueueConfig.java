package cn.ccccltd.waf.message.config;

import javax.jms.Connection;
import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.connection.SingleConnectionFactory;

import cn.ccccltd.waf.message.init.PropertyContext;

/**
 * 创建日期:2017年10月31日
 * Title:bean初始化信息
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Configuration
@EnableJms
@Order(-1)
public class BaseQueueConfig {

	/**
	 * 功能: 返回connection链接<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月6日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean
	public Connection getConnection() {
		
		Connection connection = null;
		try {
			connection = connectionFactory().createConnection();
		} catch (JMSException e) {
			e.printStackTrace();
		}
		return connection;
		
	}
	/**
	 * Spring用于管理真正的ConnectionFactory的ConnectionFactory
	 * 功能:
	 * 作者: yangjingjiang
	 * 创建日期:2017年10月31日
	 * 修改者: mender
	 * 修改日期: modifydate
	 * @return
	 */
	@Bean
	public SingleConnectionFactory connectionFactory() {
		
		SingleConnectionFactory connectionFactory = new SingleConnectionFactory();
		
		//目标ConnectionFactory对应真实的可以产生JMS Connection的ConnectionFactory
		connectionFactory.setTargetConnectionFactory(pooledConnectionFactory());
		
		return connectionFactory;
		
	}
	
	/**
	 * 功能:ActiveMQ为我们提供了一个PooledConnectionFactory，通过往里面注入一个ActiveMQConnectionFactory
	 * 可以用来将Connection、Session和MessageProducer池化，这样可以大大的减少我们的资源消耗,要依赖于 activemq-pool包<br>
	 * 作者: yangjingjiang
	 * 创建日期:2017年10月31日
	 * 修改者: mender
	 * 修改日期: modifydate
	 * @return
	 */
	@Bean
	public PooledConnectionFactory pooledConnectionFactory() {
		
		PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
		
		pooledConnectionFactory.setConnectionFactory(targetConnectionFactory());
		pooledConnectionFactory.setMaxConnections(Integer.valueOf(PropertyContext.getProperty("activemq.pool.maxConnections")));
		
		return pooledConnectionFactory;
		
	}
	
	/**
	 * 功能: 第三方MQ工厂: ConnectionFactory<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年10月31日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean
	public ActiveMQConnectionFactory targetConnectionFactory() {
		
		ActiveMQConnectionFactory amqConnectionFactory = new ActiveMQConnectionFactory();
		//配置连接地址
		amqConnectionFactory.setBrokerURL(PropertyContext.getProperty("activemq.brokerURL"));
		amqConnectionFactory.setUserName(PropertyContext.getProperty("activemq.userName"));
		amqConnectionFactory.setPassword(PropertyContext.getProperty("activemq.password"));;
		
		return amqConnectionFactory;
		
	}
	
	/**
	 * 功能:创建ContainerFactory <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年10月31日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean
	public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
		
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		
		//设置连接工厂
		factory.setConnectionFactory(connectionFactory());
	    //重连间隔时间
	    factory.setRecoveryInterval(1000L);
	    //最小连接数和最大连接数
		factory.setConcurrency("3-100");
		
		return factory;
		
	}
	
}
