package cn.ccccltd.waf.message.config;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

/**
 * 创建日期:2017年11月1日
 * Title: 事务处理类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Configuration
@Order(-3)
@EnableTransactionManagement
public class TransactionManagementConfig implements TransactionManagementConfigurer {

	@Resource(name="txManager")
    private PlatformTransactionManager txManager;

    /**
     * 功能: 创建事务管理器<br>
     * 作者: yangjingjiang <br>
     * 创建日期:2017年11月13日 <br>
     * 修改者: mender <br>
     * 修改日期: modifydate <br>
     * @param dataSource
     * @return
     */
    @Bean(name = "txManager")
    public PlatformTransactionManager txManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }


    /**
     * 功能: 实现接口 TransactionManagementConfigurer 方法，其返回值代表在拥有多个事务管理器的情况下默认使用的事务管理器<br>
     * 作者: yangjingjiang <br>
     * 创建日期:2017年11月13日 <br>
     * 修改者: mender <br>
     * 修改日期: modifydate <br>
     * @return
     * @see org.springframework.transaction.annotation.TransactionManagementConfigurer#annotationDrivenTransactionManager()
     */
    @Override
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        return txManager;
    }

}
