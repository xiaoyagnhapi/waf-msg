package cn.ccccltd.waf.message.queue.log.consumer;

import javax.annotation.Resource;
import javax.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

import cn.ccccltd.waf.message.base.consumer.ConsumerService;
import cn.ccccltd.waf.message.model.MessageCommon;
import cn.ccccltd.waf.message.model.vo.Log;
import cn.ccccltd.waf.message.queue.log.constant.LogConstants;
import cn.ccccltd.waf.message.queue.log.util.LogMessageUtils;
import cn.ccccltd.waf.message.util.JsonUtils;
import cn.ccccltd.waf.message.util.MessageUtils;
import cn.ccccltd.waf.message.util.QueueUtils;
import cn.ccccltd.waf.message.util.ToolUtils;
import cn.ccccltd.waf.message.util.sql.SqlContext;
import cn.ccccltd.waf.message.util.sql.SqlTools;

/**
 * 创建日期:2017年11月4日
 * Title: 日志记录消费者，处理日志信息的
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Component("logConsumer")
public class LogConsumerImpl<T extends MessageCommon> implements ConsumerService{

	private static Logger log = LoggerFactory.getLogger(LogConsumerImpl.class);
	
	@Resource(name="logJmsTemplate")
	private JmsTemplate logJmsTemplate;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	/**
	 * 功能: 日志处理类<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月4日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param message
	 * @see cn.ccccltd.waf.message.base.consumer.ConsumerService#receiveMessage(javax.jms.Message)
	 */
	@Override
	@JmsListener(destination=LogConstants.LOG_QUEUE, containerFactory="jmsListenerContainerFactory" )
	public void receiveMessage(Message message) {
		
		String messageText = MessageUtils.getMessageText(message);
		
		
		
		Log wml = JsonUtils.toObject(messageText, Log.class);
		wml.setId(ToolUtils.getUUID());
		wml.setMessageId(MessageUtils.getMessageStringProperty(message, LogConstants.LOG_MESSAGE_ID));
		wml.setRemoteCode(MessageUtils.getMessageStringProperty(message, LogConstants.LOG_REMOTE_CODE));
		wml.setRemoteMsg(MessageUtils.getMessageStringProperty(message, LogConstants.LOG_REMOTE_MSG));
		//查询插入的所有属性。包括父类中的
		SqlContext logInsert = SqlTools.getInsertAllProperty(wml);
		try {
			
			jdbcTemplate.update(logInsert.getSql(), logInsert.getParams());
			
		} catch (DataAccessException e) {
			log.error("cn.ccccltd.waf.message.queue.log.consumer.LogConsumerImpl.receiveMessage(Message)发生异常！");
			//将插入日志失败的日志放回队列
			QueueUtils.sendMessageToQueue(logJmsTemplate, JsonUtils.toObject(JsonUtils.toJson(wml), JSONObject.class), 
					LogMessageUtils.getSendLogMap(wml.getMessageId(), wml.getRemoteCode(), wml.getRemoteMsg()));
			e.printStackTrace();
		}
		
	}
	
}
