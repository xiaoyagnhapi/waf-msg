/**
 * <b>项目名称：</b>网络应用框架<br/>
 * <b>包    名：</b>com.hhwy.framework.datasource<br/>
 * <b>文 件 名：</b>DBAdaptation.java<br/>
 * <b>版本信息：</b><br/>
 * <b>日    期：</b>2016年11月24日-下午6:38:26<br/>
 * <b>Copyright (c)</b> 2016恒华伟业科技股份有限公司-版权所有<br/>
 * 
 */
package cn.ccccltd.waf.message.filter.sqlfilter;

 /**
  * 创建日期:2017年11月15日
  * Title: 不同数据库生成不同分页
  * Description：对本文件的详细描述，原则上不能少于50字
  * @author yangjingjiang
  * @mender：（文件的修改者，文件创建者之外的人）
  * @version 1.0
  * Remark：认为有必要的其他信息
  */
public interface DBAdaptation {
	
	/**
	 * 功能: 生成分页后的语句<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月17日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param sql
	 * @return
	 */
	public String getLimitSql(String sql);
}
