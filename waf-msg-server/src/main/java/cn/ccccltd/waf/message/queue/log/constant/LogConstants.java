package cn.ccccltd.waf.message.queue.log.constant;

/**
 * 创建日期:2017年11月4日
 * Title: 日志队列定义
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class LogConstants {

	/**日志队列的名称*/
	public static final String LOG_QUEUE = "waf.message.queue.log";
	
	/** 第三方日志消息编码 */
	public static final String LOG_REMOTE_CODE = "remote_code";
	
	/** 第三方日志消息属性 */
	public static final String LOG_REMOTE_MSG = "remote_msg";
	
	/** message的关联id */
	public static final String LOG_MESSAGE_ID = "common_id";
	
}
