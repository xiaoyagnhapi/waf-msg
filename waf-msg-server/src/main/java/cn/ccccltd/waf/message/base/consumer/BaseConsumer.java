package cn.ccccltd.waf.message.base.consumer;

import java.util.Map;

import javax.annotation.Resource;
import javax.jms.Message;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

import cn.ccccltd.waf.message.constant.MessageConstants;
import cn.ccccltd.waf.message.queue.log.constant.LogConstants;
import cn.ccccltd.waf.message.util.DateUtilsNew;
import cn.ccccltd.waf.message.util.JsonUtils;
import cn.ccccltd.waf.message.util.MessageUtils;
import cn.ccccltd.waf.message.util.QueueUtils;

/**
 * 创建日期:2017年10月30日
 * Title:发送消息消费者统一抽象类
 * Description：消费者接口统一化
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Component
public abstract class BaseConsumer {

	private static Logger log = LoggerFactory.getLogger(BaseConsumer.class);
	
	@Resource(name="logJmsTemplate")
	private JmsTemplate logJmsTemplate;
	
	
	/**
	 * 功能: 发送消息并处理错误信息<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月13日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param smsMessage
	 * @return
	 */
	protected abstract Map<String,String> sendMessage(JSONObject smsMessage);
	
	/**
	 * 功能: 获取不同的JmsTemplate<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月13日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	protected abstract JmsTemplate getJmsTemplate();
	
	/**
	 * 功能: 消费消息<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月3日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param message
	 */
	public void receiveMessage(Message message) {

		
		JSONObject smsMessage = JsonUtils.toObject(MessageUtils.getMessageText(message), JSONObject.class);
		
		/**
		 * 发送消息
		 */
		Map<String, String> sendLogMap = sendMessage(smsMessage);
		
		smsMessage.put(MessageConstants.SENDTIME, DateUtilsNew.getDateTime());
		
		log.info(MessageConstants.LOG_DEBUG_PREFIX + "消息发送！");
		
		smsMessage.put(MessageConstants.SENDTIMES, smsMessage.getInteger(MessageConstants.SENDTIMES) +1);
		//定义发送状态
		String statue  = MessageConstants.SEND_SUCCESS;
		
		
		/**
		 * 根据第三方状态码处理发送成功或者失败  sendLogMap.get(LogConstants.LOG_REMOTE_CODE);
		 */
		String remoteCode = sendLogMap.get(LogConstants.LOG_REMOTE_CODE);
		//判断是否发送成功
		boolean sendState = StringUtils.isNotBlank(remoteCode) && remoteCode.equals(HttpStatus.OK.toString());
		if (sendState) {
			
			smsMessage.put(MessageConstants.SENDSTATE, MessageConstants.SEND_SUCCESS);
			smsMessage.put(MessageConstants.DELIVEREDTIME, DateUtilsNew.getDateTime());
			

		} else {
			
			statue = MessageConstants.SEND_FAIL;
			smsMessage.put(MessageConstants.SENDSTATE, MessageConstants.SEND_FAIL_RETRY);
			
		}
		
		//让SmsUpdateTable队列更新发送状态
		QueueUtils.sendMessageToSmsUpdateTableQueue(getJmsTemplate(), smsMessage, statue);
		//发送发送消息的日志
		QueueUtils.sendMessageToQueue(logJmsTemplate, smsMessage, sendLogMap);
		//LogMessageUtils.getSendLogMap(smsMessage.getString("smsId"), sendResult.getStateCode(), sendResult.getMsg())
		log.debug(MessageConstants.LOG_DEBUG_PREFIX+"消费者消费后的消息"+JsonUtils.toJson(smsMessage));
		
	
	}
	
}
