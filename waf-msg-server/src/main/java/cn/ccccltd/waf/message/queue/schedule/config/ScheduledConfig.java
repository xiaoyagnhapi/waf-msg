package cn.ccccltd.waf.message.queue.schedule.config;

import javax.jms.ConnectionFactory;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;

import cn.ccccltd.waf.message.queue.schedule.constant.ScheduledConstants;

/**
 * 创建日期:2017年11月6日
 * Title: 创建定时消息配置类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Configuration
public class ScheduledConfig {

	@Autowired
	private ConnectionFactory connectionFactory;
	
	/**
	 * 功能: 默认取消定时后更新表的队列<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean(name = "scheduledQueue")
	public ActiveMQQueue scheduleQueue() {
		return new ActiveMQQueue(ScheduledConstants.SCHEDULED_QUEUE);
		
	}
	
	/**
	 * 功能: 取消定时后更新表的队列模板<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月6日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean(name = "scheduledJmsTemplate")
	public JmsTemplate scheduleJmsTemplate() {
		
		JmsTemplate jmsTemplate = new JmsTemplate();
		
		jmsTemplate.setConnectionFactory(connectionFactory);
		jmsTemplate.setDefaultDestinationName(ScheduledConstants.SCHEDULED_QUEUE);
		
		return jmsTemplate;
		
	}
	
}
