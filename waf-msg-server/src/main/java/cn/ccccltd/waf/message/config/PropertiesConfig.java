package cn.ccccltd.waf.message.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * 创建日期:2017年11月16日
 * Title: 配置类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Configuration
@PropertySource(value = {"/META-INF/config/waf.properties"}, ignoreResourceNotFound = true)
public class PropertiesConfig {

	/**
	 * 功能: 放置注解的方式处理配置文件<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月21日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {

		return new PropertySourcesPlaceholderConfigurer();

	}

}
