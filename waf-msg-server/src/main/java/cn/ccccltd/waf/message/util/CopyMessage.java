package cn.ccccltd.waf.message.util;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.cglib.core.Converter;

/**
 * 创建日期:2017年11月3日
 * Title:复制message工具类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class CopyMessage {

	/**cglib复制工厂*/
	private BeanCopier copier;
	/**
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月3日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 */
	public void copyBean(Object originalBean, Object newBean, Converter... converters) {
		
		if(null == copier) {
			copier = BeanCopier.create(originalBean.getClass(), newBean.getClass(), false);
		}
		
		
		Converter cv = null;
		
		if(converters.length>0) {
			cv = converters[0];
		}
		
		copier.copy(originalBean,newBean , cv);
		
	}
	
}
