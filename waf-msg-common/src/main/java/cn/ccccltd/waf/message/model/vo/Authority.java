package cn.ccccltd.waf.message.model.vo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import cn.ccccltd.waf.message.key.Key;
import cn.ccccltd.waf.message.util.ToolUtils;

/**
 * 创建日期:2017年11月15日
 * Title: 消息的权限控制
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@JsonIgnoreProperties(ignoreUnknown = true) 
@JsonInclude(Include.NON_NULL)
public class Authority implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8352202588451159937L;
	
	@Key
	private String id = ToolUtils.getUUID();
	/**注册时间*/
	private String registerTime;
	/**是否管理员*/
	private String isAdmin;
	/**申请人姓名*/
	private String applicat;
	/**手机号*/
	private String phone;
	/**邮箱*/
	private String email;
	/**系统名*/
	private String systemName;
	/**系统id*/
	private String sendSystemid;
	/**申请理由*/
	private String applyReason;
	/**审批状态*/
	private String approvalStatus = "false";
	/**审批意见*/
	private String approvalReason;
	/**开始时间*/
	private String validStart;
	/**结束时间*/
	private String validEnd;
	/**消息类型*/
	private String registerType;
	/**是否删除*/
	private String deleted;
	
	public Authority() {
		super();
	}
	public Authority(String id, String registerTime, String isAdmin, String applicat, String phone, String email,
			String systemName, String sendSystemid, String applyReason, String approvalStatus, String approvalReason,
			String validStart, String validEnd, String registerType, String deleted) {
		super();
		this.id = id;
		this.registerTime = registerTime;
		this.isAdmin = isAdmin;
		this.applicat = applicat;
		this.phone = phone;
		this.email = email;
		this.systemName = systemName;
		this.sendSystemid = sendSystemid;
		this.applyReason = applyReason;
		this.approvalStatus = approvalStatus;
		this.approvalReason = approvalReason;
		this.validStart = validStart;
		this.validEnd = validEnd;
		this.registerType = registerType;
		this.deleted = deleted;
	}
	/**
	 * @return 返回 id。
	
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id 要设置的 id。
	
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return 返回 registerTime。
	
	 */
	public String getRegisterTime() {
		return registerTime;
	}
	/**
	 * @param registerTime 要设置的 registerTime。
	
	 */
	public void setRegisterTime(String registerTime) {
		this.registerTime = registerTime;
	}
	/**
	 * @return 返回 isAdmin。
	
	 */
	public String getIsAdmin() {
		return isAdmin;
	}
	/**
	 * @param isAdmin 要设置的 isAdmin。
	
	 */
	public void setIsAdmin(String isAdmin) {
		this.isAdmin = isAdmin;
	}
	/**
	 * @return 返回 applicat。
	
	 */
	public String getApplicat() {
		return applicat;
	}
	/**
	 * @param applicat 要设置的 applicat。
	
	 */
	public void setApplicat(String applicat) {
		this.applicat = applicat;
	}
	/**
	 * @return 返回 phone。
	
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone 要设置的 phone。
	
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return 返回 email。
	
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email 要设置的 email。
	
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return 返回 systemName。
	
	 */
	public String getSystemName() {
		return systemName;
	}
	/**
	 * @param systemName 要设置的 systemName。
	
	 */
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	/**
	 * @return 返回 sendSystemid。
	
	 */
	public String getSendSystemid() {
		return sendSystemid;
	}
	/**
	 * @param sendSystemid 要设置的 sendSystemid。
	
	 */
	public void setSendSystemid(String sendSystemid) {
		this.sendSystemid = sendSystemid;
	}
	/**
	 * @return 返回 applyReason。
	
	 */
	public String getApplyReason() {
		return applyReason;
	}
	/**
	 * @param applyReason 要设置的 applyReason。
	
	 */
	public void setApplyReason(String applyReason) {
		this.applyReason = applyReason;
	}
	/**
	 * @return 返回 approvalStatus。
	
	 */
	public String getApprovalStatus() {
		return approvalStatus;
	}
	/**
	 * @param approvalStatus 要设置的 approvalStatus。
	
	 */
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	/**
	 * @return 返回 approvalReason。
	
	 */
	public String getApprovalReason() {
		return approvalReason;
	}
	/**
	 * @param approvalReason 要设置的 approvalReason。
	
	 */
	public void setApprovalReason(String approvalReason) {
		this.approvalReason = approvalReason;
	}
	/**
	 * @return 返回 validStart。
	
	 */
	public String getValidStart() {
		return validStart;
	}
	/**
	 * @param validStart 要设置的 validStart。
	
	 */
	public void setValidStart(String validStart) {
		this.validStart = validStart;
	}
	/**
	 * @return 返回 validEnd。
	
	 */
	public String getValidEnd() {
		return validEnd;
	}
	/**
	 * @param validEnd 要设置的 validEnd。
	
	 */
	public void setValidEnd(String validEnd) {
		this.validEnd = validEnd;
	}
	/**
	 * @return 返回 registerType。
	
	 */
	public String getRegisterType() {
		return registerType;
	}
	/**
	 * @param registerType 要设置的 registerType。
	
	 */
	public void setRegisterType(String registerType) {
		this.registerType = registerType;
	}
	/**
	 * @return 返回 deleted。
	
	 */
	public String getDeleted() {
		return deleted;
	}
	/**
	 * @param deleted 要设置的 deleted。
	
	 */
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	/**
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月22日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Authority [id=" + id + ", registerTime=" + registerTime + ", isAdmin=" + isAdmin + ", applicat="
				+ applicat + ", phone=" + phone + ", email=" + email + ", systemName=" + systemName + ", sendSystemid="
				+ sendSystemid + ", applyReason=" + applyReason + ", approvalStatus=" + approvalStatus
				+ ", approvalReason=" + approvalReason + ", validStart=" + validStart + ", validEnd=" + validEnd
				+ ", registerType=" + registerType + ", deleted=" + deleted + "]";
	}

}
