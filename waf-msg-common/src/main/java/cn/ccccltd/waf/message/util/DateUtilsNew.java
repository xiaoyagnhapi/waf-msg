package cn.ccccltd.waf.message.util;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

/**
 * 创建日期:2017年11月2日
 * Title:日期工具类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class DateUtilsNew extends DateUtils {

	// 日期和时间格式 =========================================================
	/** 完整日期时间格式 */
	public static final String FORMAT_DATETIME_EXT = "yyyy-MM-dd HH:mm:ss.SSS";
	/** 默认日期时间格式 */
	public static final String FORMAT_DATETIME = "yyyy-MM-dd HH:mm:ss";
	/** 默认日期格式 */
	public static final String FORMAT_DATE = "yyyy-MM-dd";
	/** 默认时间格式 */
	public static final String FORMAT_TIME = "HH:mm:ss";
	
	private static final String[] PARSE_PATTERNS = { FORMAT_DATE, FORMAT_DATETIME, "yyyy-MM-dd HH:mm", "yyyy-MM" ,FORMAT_DATETIME_EXT};

	/**
	 * 功能: 自定义返回日志格式<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String format(Date date, String pattern) {
		return DateFormatUtils.format(date, pattern);
	}

	/**
	 * 
	 * 功能: 返回年月日 yyyy-MM-dd<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param date
	 * @return
	 */
	public static String format(Date date) {
		return format(date, FORMAT_DATE);
	}

	/**
	 * 功能:  获取日期字符串(秒) yyyy-MM-dd HH:mm:ss<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param date
	 * @return
	 */
	public static String formatDateTime(Date date) {
		return format(date, FORMAT_DATETIME);
	}
	
	/**
	 * 功能: 获取日期字符串(毫秒)<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param date
	 * @return
	 */
	public static String formatDateTimeSS(Date date) {
		return format(date, FORMAT_DATETIME_EXT);
	}

	/**
	 * 功能: 获取当前日期字符串 自定义格式<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param pattern
	 * @return
	 */
	public static String formatDate(String pattern) {
		return format(new Date(), pattern);
	}

	/**
	 * 功能: 获取当前日期字符串 yyyy-MM-dd<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	public static String getDate() {
		return format(new Date(), FORMAT_DATE);
	}

	/**
	 * 功能: 获取当前时分秒字符串 HH:mm:ss<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	public static String getTime() {
		return format(new Date(), FORMAT_TIME);
	}

	/**
	 * 功能: 获取当前日期时间字符串 (yyyy-MM-dd HH:mm:ss)<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	public static String getDateTime() {
		return DateFormatUtils.format(new Date(), FORMAT_DATETIME);
	}
	
	/**
	 * 功能: 获取当前日期时间字符串 (yyyy-MM-dd HH:mm:ss.SSS)<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	public static String getDateTimeSS() {
		return DateFormatUtils.format(new Date(), FORMAT_DATETIME_EXT);
	}

	/**
	 * 功能: 获取当前年份字符串(year)<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	public static String getYear() {
		return format(new Date(), "yyyy");
	}

	/**
	 * 功能: 获取当前月份字符串(month) <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	public static String getMonth() {
		return format(new Date(), "MM");
	}

	/**
	 * 功能: 获取当前天字符串 (day)<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	public static String getDay() {
		return format(new Date(), "dd");
	}

	/**
	 * 功能: 获取当前星期字符串 (week)<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	public static String getWeek() {
		return format(new Date(), "E");
	}
	
	/**
	 * 功能: 获取当前日期<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */

	public static Date getNow() {
		return new Date();
	}

	/**
	 * 功能: 日期型字符串转化为日期<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param str
	 * @return
	 */
	public static Date parseDate(String str) {
		if (str == null) {
			return null;
		}
		try {
			return parseDate(str, PARSE_PATTERNS);
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * 功能: 获取两个日期之间的天数<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param before
	 * @param after
	 * @return
	 */
	public static double getDistanceOfTwoDate(Date before, Date after) {
		long beforeTime = before.getTime();
		long afterTime = after.getTime();
		return (afterTime - beforeTime) / (1000 * 60 * 60 * 24);
	}

}
