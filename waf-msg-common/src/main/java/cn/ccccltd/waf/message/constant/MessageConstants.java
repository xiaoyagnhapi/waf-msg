package cn.ccccltd.waf.message.constant;

/**
 * 创建日期:2017年11月2日
 * Title:日志打印通用开头
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class MessageConstants {
	
	/**消息前缀*/
	public static final String MESSAGE = "message";
	
	/**初始化无需过滤的查询*/
	public static boolean INIT_DATA = true;
	
	/** 人为打印log日志前缀 */
	public static final String LOG_DEBUG_PREFIX = "======";
	
	public static final String SELECTOR = "selector";
	
	/**发送状态*/
	public static final String SEND_STATUE_PROPERTY = "sendStatue";
	
	/**待发送*/
	public static final String SEND_WAIT = "00";
	/**发送成功*/
	public static final String SEND_SUCCESS = "100";
	/**发送失败*/
	public static final String SEND_FAIL = "200";
	/**发送失败，正在重试*/
	public static final String SEND_FAIL_RETRY = "201";
	/**已取消发送*/
	public static final String SEND_FAIL_CANCEL = "300";
	/**没有权限访问*/
	public static final String ERR_NOT_PERMISSION = "555";
	/**主键重复错误码*/
	public static final String ERR_HAS_DUPLICATE = "444";
	
	/**消息-紧急属性*/
	public static final int SEND_URGENT_MESSAGE = 9;
	/**消息-紧急*/
	public static final String URGENT_MESSAGE = "0002";
	
	/**没有权限访问*/
	public static final String SESSION = "session_message";
	
	/**成功*/
	public static final String TRUE = "true";
	/**失败*/
	public static final String FALSE = "false";
	
	/**成功*/
	public static final String TRUE_CHAR = "1";
	/**失败*/
	public static final String FALSE_CHAR = "0";
	
	/**
	 * 消息公用字段
	 */
	/**消息-发送时间*/
	public static final String SENDTIME = "sendTime";
	
	/**消息-发送次数*/
	public static final String SENDTIMES = "sendTimes";
	
	/**消息-发送状态*/
	public static final String SENDSTATE = "sendState";
	
	/**消息-成功送达时间*/
	public static final String DELIVEREDTIME = "deliveredTime";
	
	/**消息-消息等级*/
	public static final String MSGSCOPE = "msgScope";
	
	/**消息-消息类别*/
	public static final String CATEGORY = "category";
	
	/**消息-id*/
	public static final String ID = "id";
	/**
	 * 数据过滤
	 */
	
	/**传递数据封装的对象*/
	public static final String JSON = "json";
	/**是否过滤数据*/
	public static final String ISFILTERDATA = "isFilterData";
	/**发送系统id*/
	public static final String SENDSYSTEMID = "sendSystemid";
	/**发送人id*/
	public static final String SENDUSERID = "sendUserid";
	/**查询项*/
	public static final String QUERYITEMS = "queryItems";
	/**排序*/
	public static final String ORDERS = "orders";
	/**一页数据量*/
	public static final String RPP = "rpp";
	/**页号*/
	public static final String PNO = "pno";
	/**无需过滤的查询*/
	public static final String NOTFILTERDATA = "notFilterData";
	
	
	/**短信错误信息过滤条件*/
	public static final String ERROR_MESSAGE_SELECTOR = "ErrorMessage";
	
	
	// 关系符 ==============================================================
    /** 关系符: 等于*/
    public static final String RELATION_EQ    = "eq";
    /** 关系符: 大于*/
    public static final String RELATION_GT    = "gt";
    /** 关系符: 大于等于*/
    public static final String RELATION_GE    = "ge";
    /** 关系符: 小于*/
    public static final String RELATION_LT    = "lt";
    /** 关系符: 小于等于*/
    public static final String RELATION_LE    = "le";
    /** 关系符: 包含*/
    public static final String RELATION_LIKE  = "like";
    /** 关系符: 左包含*/
    public static final String RELATION_LLIKE = "llike";
    /** 关系符: 右包含*/
    public static final String RELATION_RLIKE = "rlike";
}