package cn.ccccltd.waf.message.model.params;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 创建日期:2017年11月13日
 * Title: 查询参数
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@JsonIgnoreProperties(ignoreUnknown = true) 
@JsonInclude(Include.NON_NULL)
public class QueryParams implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2951491463236466540L;
	
	/**用户id*/
	private String sendUserid;
	/**系统id*/
	private String sendSystemid;
	/**业务模块*/
	private String businessModule;
	/**类别*/
	private String category;
	
	/**类别*/
	private Map<String, Integer> page;
	
	/**查询项*/
	private List<QueryItem> queryItems;
	/**排序*/
	private List<Order> orders;
	
	public QueryParams() {
		super();
	}

	public QueryParams(String sendUserid, String sendSystemid, String businessModule, String category,
			Map<String, Integer> page, List<QueryItem> queryItems, List<Order> orders) {
		super();
		this.sendUserid = sendUserid;
		this.sendSystemid = sendSystemid;
		this.businessModule = businessModule;
		this.category = category;
		this.page = page;
		this.queryItems = queryItems;
		this.orders = orders;
	}

	/**
	 * @return 返回 sendUserid。
	
	 */
	public String getSendUserid() {
		return sendUserid;
	}

	/**
	 * @param sendUserid 要设置的 sendUserid。
	
	 */
	public void setSendUserid(String sendUserid) {
		this.sendUserid = sendUserid;
	}

	/**
	 * @return 返回 sendSystemid。
	
	 */
	public String getSendSystemid() {
		return sendSystemid;
	}

	/**
	 * @param sendSystemid 要设置的 sendSystemid。
	
	 */
	public void setSendSystemid(String sendSystemid) {
		this.sendSystemid = sendSystemid;
	}

	/**
	 * @return 返回 businessModule。
	
	 */
	public String getBusinessModule() {
		return businessModule;
	}

	/**
	 * @param businessModule 要设置的 businessModule。
	
	 */
	public void setBusinessModule(String businessModule) {
		this.businessModule = businessModule;
	}

	/**
	 * @return 返回 category。
	
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category 要设置的 category。
	
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return 返回 page。
	
	 */
	public Map<String, Integer> getPage() {
		return page;
	}

	/**
	 * @param page 要设置的 page。
	
	 */
	public void setPage(Map<String, Integer> page) {
		this.page = page;
	}

	/**
	 * @return 返回 queryItems。
	
	 */
	public List<QueryItem> getQueryItems() {
		return queryItems;
	}

	/**
	 * @param queryItems 要设置的 queryItems。
	
	 */
	public void setQueryItems(List<QueryItem> queryItems) {
		this.queryItems = queryItems;
	}

	/**
	 * @return 返回 orders。
	
	 */
	public List<Order> getOrders() {
		return orders;
	}

	/**
	 * @param orders 要设置的 orders。
	
	 */
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	/**
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月13日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "QueryParams [sendUserid=" + sendUserid + ", sendSystemid=" + sendSystemid + ", businessModule="
				+ businessModule + ", category=" + category + ", page=" + page + ", queryItems=" + queryItems
				+ ", orders=" + orders + "]";
	}

	
}
