package cn.ccccltd.waf.message.util;

import java.util.Arrays;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * 创建日期:2017年10月31日
 * Title:uuid获取工具类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class ToolUtils {

	
	/**邮箱*/
	private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	/**手机号*/
	private static final String CHINA_REGEX = "^((13[0-9])|(15[^4])|(18[0,2,3,5-9])|(17[0-8])|(147))\\d{8}$";
	private static final String HK_REGEX = "^(5|6|8|9)\\d{7}$";

    /**
     * 功能: 大陆号码或香港号码均可<br>
     * 作者: yangjingjiang <br>
     * 创建日期:2017年11月17日 <br>
     * 修改者: mender <br>
     * 修改日期: modifydate <br>
     * @param str
     * @return
     * @throws PatternSyntaxException
     */
    public static boolean isPhone(String str)throws PatternSyntaxException {    
        return isChinaPhoneLegal(str) || isHKPhoneLegal(str);    
    }    
    
    public static void main(String[] args) {
		System.out.println(isPhone("13971157172"));
	}
    
    /**
     * 功能:     
     * 大陆手机号码11位数，匹配格式：前三位固定格式+后8位任意数  
     * 此方法中前三位格式有：  
     * 13+任意数  
     * 15+除4的任意数  
     * 18+除1和4的任意数  
     * 17+除9的任意数  
     * 147<br>
     * 作者: yangjingjiang <br>
     * 创建日期:2017年11月17日 <br>
     * 修改者: mender <br>
     * 修改日期: modifydate <br>
     * @param str
     * @return
     * @throws PatternSyntaxException
     */
    public static boolean isChinaPhoneLegal(String str) throws PatternSyntaxException {  
    	
        return Pattern.matches(CHINA_REGEX, str); 
    	
    }    
    
    /**
     * 功能: 香港手机号码8位数，5|6|8|9开头+7位任意数  <br>
     * 作者: yangjingjiang <br>
     * 创建日期:2017年11月17日 <br>
     * 修改者: mender <br>
     * 修改日期: modifydate <br>
     * @param str
     * @return
     * @throws PatternSyntaxException
     */
    public static boolean isHKPhoneLegal(String str)throws PatternSyntaxException {    

    	return Pattern.matches(HK_REGEX, str);
        
    }  
	
	/**
	 * 功能: 邮箱格式校验<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月18日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param email
	 * @return
	 */
	public static boolean isEmail(String email) {
		
		return Pattern.matches(EMAIL_REGEX, email);
		
	}
	
	/**
	 * 功能: 返回32位的UUID<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年10月31日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	public static String getUUID(){
		
		return UUID.randomUUID().toString().replace("-", "");
		
	}
	
	/**
	 * 数组合并
	 * @param first
	 * @param second
	 * @return
	 */
	public static Object[] concat(Object[] first, Object second) {
		Object[] result = Arrays.copyOf(first, first.length + 1);
		System.arraycopy(new Object[]{second}, 0, result, first.length, 1);
		return result;
	}

	/**
	 * 数组合并
	 * @param first
	 * @param second
	 * @return
	 */
	public static Object[] concat(Object[] first, Object... second) {
		Object[] result = new Object[first.length + second.length];
		System.arraycopy(first, 0, result, 0, first.length);
		System.arraycopy(second, 0, result, first.length, second.length);
		return result;
	}
	
	
}
