package cn.ccccltd.waf.message.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import cn.ccccltd.waf.message.key.Key;
import cn.ccccltd.waf.message.model.MessageCommon;
import cn.ccccltd.waf.message.util.ToolUtils;

/**
 * 创建日期:2017年11月14日
 * Title: 邮件消息
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@JsonIgnoreProperties(ignoreUnknown = true) 
@JsonInclude(Include.NON_NULL)
public class MessageEmail  extends MessageCommon{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1666553627020147182L;
	/**主键*/
	@Key
	private String emailId = ToolUtils.getUUID();
	/**父关联id*/
	private String commonId;
	/**邮件内容*/
	private String content;
	/**邮箱地址*/
	private String emailAddress;
	/**发送次数*/
	private Integer sendTimes = 0;
	/**发送状态(必须每个消息中都含有这个字段)
	 * 00为待发送
	 * 100为发送成功
	 * 201为发送失败正在重试
	 * 300为已取消发送
	 * */
	private String sendState = "00";
	/**成功送达时间*/
	private String deliveredTime;
	public MessageEmail() {
		super();
	}
	public MessageEmail(String id, String sendSystemid, String createTime, String businessModule,
			String sendUserid, String sendTime, String category, String msgScope, String msgsendType, String msgTopic) {
		super(id, sendSystemid, createTime, businessModule, sendUserid, sendTime, category, msgScope, msgsendType, msgTopic);
	}
	public MessageEmail(String emailId, String commonId, String content, String emailAddress, Integer sendTimes,
			String sendState, String deliveredTime) {
		super();
		this.emailId = emailId;
		this.commonId = commonId;
		this.content = content;
		this.emailAddress = emailAddress;
		this.sendTimes = sendTimes;
		this.sendState = sendState;
		this.deliveredTime = deliveredTime;
	}
	/**
	 * @return 返回 emailId。
	
	 */
	public String getEmailId() {
		return emailId;
	}
	/**
	 * @param emailId 要设置的 emailId。
	
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	/**
	 * @return 返回 commonId。
	
	 */
	public String getCommonId() {
		return commonId;
	}
	/**
	 * @param commonId 要设置的 commonId。
	
	 */
	public void setCommonId(String commonId) {
		this.commonId = commonId;
	}
	/**
	 * @return 返回 content。
	
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content 要设置的 content。
	
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return 返回 emailAddress。
	
	 */
	public String getEmailAddress() {
		return emailAddress;
	}
	/**
	 * @param emailAddress 要设置的 emailAddress。
	
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	/**
	 * @return 返回 sendTimes。
	
	 */
	public Integer getSendTimes() {
		return sendTimes;
	}
	/**
	 * @param sendTimes 要设置的 sendTimes。
	
	 */
	public void setSendTimes(Integer sendTimes) {
		this.sendTimes = sendTimes;
	}
	/**
	 * @return 返回 sendState。
	
	 */
	public String getSendState() {
		return sendState;
	}
	/**
	 * @param sendState 要设置的 sendState。
	
	 */
	public void setSendState(String sendState) {
		this.sendState = sendState;
	}
	/**
	 * @return 返回 deliveredTime。
	
	 */
	public String getDeliveredTime() {
		return deliveredTime;
	}
	/**
	 * @param deliveredTime 要设置的 deliveredTime。
	
	 */
	public void setDeliveredTime(String deliveredTime) {
		this.deliveredTime = deliveredTime;
	}
	/**
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月14日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MessageEmail [emailId=" + emailId + ", commonId=" + commonId + ", content=" + content
				+ ", emailAddress=" + emailAddress + ", sendTimes=" + sendTimes + ", sendState=" + sendState
				+ ", deliveredTime=" + deliveredTime + "]";
	}
	
	
	
}
