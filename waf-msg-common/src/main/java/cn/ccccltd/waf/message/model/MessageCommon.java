package cn.ccccltd.waf.message.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import cn.ccccltd.waf.message.key.Key;
import cn.ccccltd.waf.message.util.DateUtilsNew;
import cn.ccccltd.waf.message.util.ToolUtils;

/**
 * 创建日期:2017年10月31日
 * Title: 消息的父类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@JsonIgnoreProperties(ignoreUnknown = true) 
@JsonInclude(Include.NON_NULL)
public class MessageCommon implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4429541187909890802L;
	/**主键*/
	@Key
	private String id = ToolUtils.getUUID();
	/**发送系统id*/
	private String sendSystemid;
	/**创建时间*/
	private String createTime = DateUtilsNew.getDateTime();
	/**业务系统模块*/
	private String businessModule;
	/**发送人id*/
	private String sendUserid;
	/**发送时间*/
	private String sendTime;
	/**消息类型 */
	private String category;
	/**消息重要等级
	 * 最低0  重发一次
	 * 默认4
	 * 紧急9
	 * */
	private String msgScope = "1";
	/**消息传送模式
	 * 默认0001
	 * 0001点对点0002订阅模式*/
	private String msgsendType = "0001";
	/**消息主题*/
	private String msgTopic;
	
	public MessageCommon() {
		super();
	}

	public MessageCommon(String id, String sendSystemid, String createTime, String businessModule,
			String sendUserid, String sendTime, String category, String msgScope, String msgsendType, String msgTopic) {
		super();
		this.id = id;
		this.sendSystemid = sendSystemid;
		this.createTime = createTime;
		this.businessModule = businessModule;
		this.sendUserid = sendUserid;
		this.sendTime = sendTime;
		this.category = category;
		this.msgScope = msgScope;
		this.msgsendType = msgsendType;
		this.msgTopic = msgTopic;
	}
	
	/**
	 * @return 返回 id。
	
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id 要设置的 id。
	
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return 返回 sendSystemid。
	
	 */
	public String getSendSystemid() {
		return sendSystemid;
	}

	/**
	 * @param sendSystemid 要设置的 sendSystemid。
	
	 */
	public void setSendSystemid(String sendSystemid) {
		this.sendSystemid = sendSystemid;
	}

	/**
	 * @return 返回 businessModule。
	
	 */
	public String getBusinessModule() {
		return businessModule;
	}

	/**
	 * @param businessModule 要设置的 businessModule。
	
	 */
	public void setBusinessModule(String businessModule) {
		this.businessModule = businessModule;
	}

	/**
	 * @return 返回 sendUserid。
	
	 */
	public String getSendUserid() {
		return sendUserid;
	}

	/**
	 * @param sendUserid 要设置的 sendUserid。
	
	 */
	public void setSendUserid(String sendUserid) {
		this.sendUserid = sendUserid;
	}

	/**
	 * @return 返回 sendTime。
	
	 */
	public String getSendTime() {
		return sendTime;
	}

	/**
	 * @param sendTime 要设置的 sendTime。
	
	 */
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	/**
	 * @return 返回 category。
	
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category 要设置的 category。
	
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return 返回 msgScope。
	
	 */
	public String getMsgScope() {
		return msgScope;
	}

	/**
	 * @param msgScope 要设置的 msgScope。
	
	 */
	public void setMsgScope(String msgScope) {
		this.msgScope = msgScope;
	}

	/**
	 * @return 返回 msgsendType。
	
	 */
	public String getMsgsendType() {
		return msgsendType;
	}

	/**
	 * @param msgsendType 要设置的 msgsendType。
	
	 */
	public void setMsgsendType(String msgsendType) {
		this.msgsendType = msgsendType;
	}

	/**
	 * @return 返回 msgTopic。
	
	 */
	public String getMsgTopic() {
		return msgTopic;
	}

	/**
	 * @param msgTopic 要设置的 msgTopic。
	
	 */
	public void setMsgTopic(String msgTopic) {
		this.msgTopic = msgTopic;
	}

	/**
	 * @return 返回 createTime。
	
	 */
	public String getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime 要设置的 createTime。
	
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	/**
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月10日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MessageCommon [id=" + id + ", sendSystemid=" + sendSystemid + ", createTime=" + createTime
				+ ", businessModule=" + businessModule + ", sendUserid=" + sendUserid + ", sendTime=" + sendTime
				+ ", category=" + category + ", msgScope=" + msgScope + ", msgsendType=" + msgsendType + ", msgTopic="
				+ msgTopic + "]";
	}
	
}
