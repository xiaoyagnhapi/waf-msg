package cn.ccccltd.waf.message.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import cn.ccccltd.waf.message.key.Key;
import cn.ccccltd.waf.message.model.MessageCommon;
import cn.ccccltd.waf.message.util.ToolUtils;

/**
 * 创建日期:2017年10月31日
 * Title:短信消息类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@JsonIgnoreProperties(ignoreUnknown = true) 
@JsonInclude(Include.NON_NULL)
public class MessageSms extends MessageCommon{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -131274874293897609L;
	/**主键*/
	@Key
	private String smsId = ToolUtils.getUUID();
	/**父关联id*/
	private String commonId;
	/**手机号*/
	private String phoneNumber;
	/**发送次数*/
	private Integer sendTimes = 0;
	/**发送状态(必须每个消息中都含有这个字段)
	 * 00为待发送
	 * 100为发送成功
	 * 201为发送失败正在重试
	 * 300为已取消发送
	 * */
	private String sendState = "00";
	/**成功送达时间*/
	private String deliveredTime;

	

	public MessageSms() {
		super();
	}

	public MessageSms(String id, String sendSystemid, String createTime, String businessModule, String sendUserid,
			String sendTime, String category, String msgScope, String msgsendType, String msgTopic) {
		super(id, sendSystemid, createTime, businessModule, sendUserid, sendTime, category, msgScope, msgsendType, msgTopic);
	}

	public MessageSms(String smsId, String commonId, String phoneNumber, Integer sendTimes, String sendState,
			String deliveredTime) {
		super();
		this.smsId = smsId;
		this.commonId = commonId;
		this.phoneNumber = phoneNumber;
		this.sendTimes = sendTimes;
		this.sendState = sendState;
		this.deliveredTime = deliveredTime;
	}

	/**
	 * @return 返回 smsId。
	
	 */
	public String getSmsId() {
		return smsId;
	}
	/**
	 * @param smsId 要设置的 smsId。
	
	 */
	public void setSmsId(String smsId) {
		this.smsId = smsId;
	}
	/**
	 * @return 返回 commonId。
	
	 */
	public String getCommonId() {
		return commonId;
	}
	/**
	 * @param commonId 要设置的 commonId。
	
	 */
	public void setCommonId(String commonId) {
		this.commonId = commonId;
	}
	/**
	 * @return 返回 phoneNumber。
	
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @param phoneNumber 要设置的 phoneNumber。
	
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @return 返回 sendTimes。
	
	 */
	public Integer getSendTimes() {
		return sendTimes;
	}

	/**
	 * @param sendTimes 要设置的 sendTimes。
	
	 */
	public void setSendTimes(Integer sendTimes) {
		this.sendTimes = sendTimes;
	}

	/**
	 * @return 返回 sendState。
	
	 */
	public String getSendState() {
		return sendState;
	}
	/**
	 * @param sendState 要设置的 sendState。
	
	 */
	public void setSendState(String sendState) {
		this.sendState = sendState;
	}
	/**
	 * @return 返回 deliveredTime。
	
	 */
	public String getDeliveredTime() {
		return deliveredTime;
	}
	/**
	 * @param deliveredTime 要设置的 deliveredTime。
	
	 */
	public void setDeliveredTime(String deliveredTime) {
		this.deliveredTime = deliveredTime;
	}
	/**
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年10月31日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SmsMessageVo [smsId=" + smsId + ", commonId=" + commonId + ", phoneNumber=" + phoneNumber
				+ ", sendTimes=" + sendTimes + ", sendState=" + sendState + ", deliveredTime=" + deliveredTime + "]";
	}

}
