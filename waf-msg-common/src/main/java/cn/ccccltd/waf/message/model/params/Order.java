package cn.ccccltd.waf.message.model.params;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 创建日期:2017年11月17日
 * Title: 排序处理
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@JsonIgnoreProperties(ignoreUnknown = true) 
@JsonInclude(Include.NON_NULL)
public class Order implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2691678430516334131L;
	/**排序字段*/
	private String sortName;
	/**排序类型*/
	private String sortType;
	
	public Order() {
		super();
	}

	public Order(String sortName, String sortType) {
		super();
		this.sortName = sortName;
		this.sortType = sortType;
	}

	/**
	 * @return 返回 sortName。
	
	 */
	public String getSortName() {
		return sortName;
	}

	/**
	 * @param sortName 要设置的 sortName。
	
	 */
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}

	/**
	 * @return 返回 sortType。
	
	 */
	public String getSortType() {
		return sortType;
	}

	/**
	 * @param sortType 要设置的 sortType。
	
	 */
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}

	/**
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月13日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Order [sortName=" + sortName + ", sortType=" + sortType + "]";
	}
	
}
