package cn.ccccltd.waf.message.util;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 创建日期:2017年11月1日
 * Title:使用jackson来转换，jackson性能比fastjson好
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class JsonUtils {


	private static ObjectMapper mapper = new ObjectMapper();
	

	/**
	 * 功能: 把对象转换成json<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月1日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param value
	 * @return
	 */
	public static String toJson(Object value) {
		try {
			return mapper.writeValueAsString(value);
		} catch (Exception e) {
			throw new RuntimeException("json转换异常！", e);
		}
	}

	/**
	 * 功能: 把json转换成功对象<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月1日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param json
	 * @param clazz
	 * @return
	 */
	public static <T> T toObject(String json, Class<T> clazz) {
		try {
			return mapper.readValue(json, clazz);
		} catch (Exception e) {
			throw new RuntimeException("json转换对象异常", e);
		}
	}

	/**
	 * 功能: 把json转换成list<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月1日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param json
	 * @param valueTypeRef
	 * @return
	 */
	public static List<?> toList(String json,
			TypeReference<?> valueTypeRef) {
		try {
			return mapper.readValue(json, valueTypeRef);
		} catch (Exception e) {
			throw new RuntimeException("json转换list异常！", e);
		}
	}

}
