package cn.ccccltd.waf.message.model.result;

import java.util.List;

/**
 * 创建日期:2017年11月13日
 * Title: 数据集合
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 * @param <T>
 */
public class Data<T> {
	
	/**数据*/
	private List<T> rows = null;
	
	/**总记录数*/
	private Long total = null;
	
	public Data() {
		super();
	}

	public Data(List<T> rows, Long total) {
		super();
		this.rows = rows;
		this.total = total;
	}

	/**
	 * @return 返回 rows。
	
	 */
	public List<T> getRows() {
		return rows;
	}

	/**
	 * @param rows 要设置的 rows。
	
	 */
	public void setRows(List<T> rows) {
		this.rows = rows;
	}

	/**
	 * @return 返回 total。
	
	 */
	public Long getTotal() {
		return total;
	}

	/**
	 * @param total 要设置的 total。
	
	 */
	public void setTotal(Long total) {
		this.total = total;
	}

	/**
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月11日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Data [rows=" + rows + ", total=" + total + "]";
	}

}
