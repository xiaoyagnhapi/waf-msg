package cn.ccccltd.waf.message.model.vo;

import java.io.Serializable;

import cn.ccccltd.waf.message.model.MessageCommon;

/**
 * 创建日期:2017年11月11日
 * Title: 总括信息
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class GeneralMessage extends MessageCommon implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7759756310983149554L;
	
	/**总数*/
	private String total;
	/**等待数*/
	private String waitNum;
	/**成功数*/
	private String successNum;
	/**失败数*/
	private String failNum;
	/**重试数*/
	private String retryNum;
	/**取消发送的数量*/
	private String canlNum;
	/**公共id*/
	private String commonId;

	public GeneralMessage() {
		super();
	}

	public GeneralMessage(String total, String waitNum, String successNum, String failNum, String retryNum,
			String canlNum, String commonId) {
		super();
		this.total = total;
		this.waitNum = waitNum;
		this.successNum = successNum;
		this.failNum = failNum;
		this.retryNum = retryNum;
		this.canlNum = canlNum;
		this.commonId = commonId;
	}

	/**
	 * @return 返回 total。
	
	 */
	public String getTotal() {
		return total;
	}

	/**
	 * @param total 要设置的 total。
	
	 */
	public void setTotal(String total) {
		this.total = total;
	}

	/**
	 * @return 返回 waitNum。
	
	 */
	public String getWaitNum() {
		return waitNum;
	}

	/**
	 * @param waitNum 要设置的 waitNum。
	
	 */
	public void setWaitNum(String waitNum) {
		this.waitNum = waitNum;
	}

	/**
	 * @return 返回 successNum。
	
	 */
	public String getSuccessNum() {
		return successNum;
	}

	/**
	 * @param successNum 要设置的 successNum。
	
	 */
	public void setSuccessNum(String successNum) {
		this.successNum = successNum;
	}

	/**
	 * @return 返回 failNum。
	
	 */
	public String getFailNum() {
		return failNum;
	}

	/**
	 * @param failNum 要设置的 failNum。
	
	 */
	public void setFailNum(String failNum) {
		this.failNum = failNum;
	}

	/**
	 * @return 返回 retryNum。
	
	 */
	public String getRetryNum() {
		return retryNum;
	}

	/**
	 * @param retryNum 要设置的 retryNum。
	
	 */
	public void setRetryNum(String retryNum) {
		this.retryNum = retryNum;
	}

	/**
	 * @return 返回 canlNum。
	
	 */
	public String getCanlNum() {
		return canlNum;
	}

	/**
	 * @param canlNum 要设置的 canlNum。
	
	 */
	public void setCanlNum(String canlNum) {
		this.canlNum = canlNum;
	}

	/**
	 * @return 返回 commonId。
	
	 */
	public String getCommonId() {
		return commonId;
	}

	/**
	 * @param commonId 要设置的 commonId。
	
	 */
	public void setCommonId(String commonId) {
		this.commonId = commonId;
	}

	/**
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月22日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GeneralMessage [total=" + total + ", waitNum=" + waitNum + ", successNum=" + successNum + ", failNum="
				+ failNum + ", retryNum=" + retryNum + ", canlNum=" + canlNum + ", commonId=" + commonId + "]";
	}

	
	
}
