package cn.ccccltd.waf.message.model.result;

import java.io.Serializable;

/**
 * 创建日期:2017年11月11日
 * Title: 返回结构数据集合
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class MessageResult implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1293842775873106599L;
	
	/**成功或失败*/
	private String success;
	/**返回的状态码*/
	private String stateCode;
	/**返回的提示信息*/
	private String msg;
	/**具体返回的数据*/
	private Object data;
	
	public MessageResult() {
		super();
	}

	public MessageResult(String success, String stateCode, String msg, Object data) {
		super();
		this.success = success;
		this.stateCode = stateCode;
		this.msg = msg;
		this.data = data;
	}

	public MessageResult(String success, String stateCode, String msg) {
		super();
		this.success = success;
		this.stateCode = stateCode;
		this.msg = msg;
	}

	/**
	 * @return 返回 success。
	
	 */
	public String getSuccess() {
		return success;
	}

	/**
	 * @param success 要设置的 success。
	
	 */
	public void setSuccess(String success) {
		this.success = success;
	}

	/**
	 * @return 返回 stateCode。
	
	 */
	public String getStateCode() {
		return stateCode;
	}

	/**
	 * @param stateCode 要设置的 stateCode。
	
	 */
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	/**
	 * @return 返回 msg。
	
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg 要设置的 msg。
	
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @return 返回 data。
	
	 */
	public Object getData() {
		return data;
	}

	/**
	 * @param data 要设置的 data。
	
	 */
	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月13日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MessageResult [success=" + success + ", stateCode=" + stateCode + ", msg=" + msg + ", data=" + data + "]";
	}
	
}
