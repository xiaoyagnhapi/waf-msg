package cn.ccccltd.waf.message.model.vo;

import cn.ccccltd.waf.message.model.MessageCommon;

/**
 * 创建日期:2017年11月4日
 * Title: 日志实体
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class Log extends MessageCommon{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5934190536863279106L;

	/**消息表id*/
	private String messageId;
	/**第三方日志返回的状态码*/
	private String remoteCode;
	/**调用第三方返回的日志信息*/
	private String remoteMsg;
	/**发送次数*/
	private Integer sendTimes;
	/**发送状态*/
	private String sendState;
	
	public Log() {
		super();
	}
	
	public Log(String id, String sendSystemid, String createTime, String businessModule, String sendUserid,
			String sendTime, String category, String msgScope, String msgsendType, String msgTopic) {
		super(id, sendSystemid, createTime, businessModule, sendUserid, sendTime, category, msgScope, msgsendType, msgTopic);
	}

	
	public Log(String messageId, String remoteCode, String remoteMsg, Integer sendTimes, String sendState) {
		super();
		this.messageId = messageId;
		this.remoteCode = remoteCode;
		this.remoteMsg = remoteMsg;
		this.sendTimes = sendTimes;
		this.sendState = sendState;
	}

	/**
	 * @return 返回 messageId。
	
	 */
	public String getMessageId() {
		return messageId;
	}
	/**
	 * @param messageId 要设置的 messageId。
	
	 */
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	/**
	 * @return 返回 remoteCode。
	
	 */
	public String getRemoteCode() {
		return remoteCode;
	}
	/**
	 * @param remoteCode 要设置的 remoteCode。
	
	 */
	public void setRemoteCode(String remoteCode) {
		this.remoteCode = remoteCode;
	}
	/**
	 * @return 返回 remoteMsg。
	
	 */
	public String getRemoteMsg() {
		return remoteMsg;
	}
	/**
	 * @param remoteMsg 要设置的 remoteMsg。
	
	 */
	public void setRemoteMsg(String remoteMsg) {
		this.remoteMsg = remoteMsg;
	}
	/**
	 * @return 返回 sendTimes。
	
	 */
	public Integer getSendTimes() {
		return sendTimes;
	}
	/**
	 * @param sendTimes 要设置的 sendTimes。
	
	 */
	public void setSendTimes(Integer sendTimes) {
		this.sendTimes = sendTimes;
	}
	/**
	 * @return 返回 sendState。
	
	 */
	public String getSendState() {
		return sendState;
	}
	/**
	 * @param sendState 要设置的 sendState。
	
	 */
	public void setSendState(String sendState) {
		this.sendState = sendState;
	}

	/**
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月10日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Log [messageId=" + messageId + ", remoteCode=" + remoteCode + ", remoteMsg=" + remoteMsg
				+ ", sendTimes=" + sendTimes + ", sendState=" + sendState + "]";
	}


}
