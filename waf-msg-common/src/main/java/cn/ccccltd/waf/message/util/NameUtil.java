package cn.ccccltd.waf.message.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * 创建日期:2017年11月1日
 * Title:驼峰转换工具类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */

public class NameUtil {
	
	private static final String UNDERLINE = "_";
	
	private static final String TABLE_PREFIX = "waf_msg";
	
	private static Pattern linePattern = Pattern.compile("_(\\w)");

	/**
	 * 功能: 获取下划线的命名<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月21日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param param
	 * @return
	 */
	public static String getUnderLineName(String param) {
		
		boolean firstIsUpperCase = false;
		
		if (StringUtils.isBlank(param)) {
			return "";
		}
		int len = param.length();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			char c = param.charAt(i);
			if (Character.isUpperCase(c)) {
				if (0==i) {
					firstIsUpperCase = true;
				}
				sb.append(UNDERLINE);
				sb.append(Character.toLowerCase(c));
			} else {
				sb.append(c);
			}
		}
		if(true == firstIsUpperCase) {
			return sb.toString().substring(1);
		}
		return sb.toString().substring(0);
	}
	
	/**
	 * 功能: 获取驼峰式命名<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月21日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param str
	 * @return
	 */
	public static String getCamelName(String str) {
		if(!str.contains(UNDERLINE)) {
			return str;
		}
		str = str.toLowerCase();  
        Matcher matcher = linePattern.matcher(str);  
        StringBuffer sb = new StringBuffer();  
        while(matcher.find()){  
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());  
        }  
        matcher.appendTail(sb);  
        return sb.toString();  
	}
	
	/**
	 * 功能: 获取表名称<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月21日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param param
	 * @return
	 */
	public static String getTableName(String param) {
		
		if (StringUtils.isBlank(param)) {
			return "";
		}
		int len = param.length();
		StringBuilder sb = new StringBuilder(len);
		//表的公共前缀
		sb.append(TABLE_PREFIX);
		for (int i = 0; i < len; i++) {
			char c = param.charAt(i);
			if (Character.isUpperCase(c)) {
				sb.append(UNDERLINE);
				sb.append(Character.toLowerCase(c));
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}

}
