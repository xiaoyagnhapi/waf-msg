package cn.ccccltd.waf.message.model.params;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 创建日期:2017年11月13日
 * Title: 查询项
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@JsonIgnoreProperties(ignoreUnknown = true) 
@JsonInclude(Include.NON_NULL)
public class QueryItem implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2430976441592655707L;
	/**列*/
	private String colwhere;
	/**查询值*/
	private String value;
	/**查询条件*/
	private String relation;
	
	private String logic = " AND ";

	public QueryItem() {
		super();
	}

	public QueryItem(String colwhere, String value, String relation, String logic) {
		super();
		this.colwhere = colwhere;
		this.value = value;
		this.relation = relation;
		this.logic = logic;
	}

	/**
	 * @return 返回 colwhere。
	
	 */
	public String getColwhere() {
		return colwhere;
	}

	/**
	 * @param colwhere 要设置的 colwhere。
	
	 */
	public void setColwhere(String colwhere) {
		this.colwhere = colwhere;
	}

	/**
	 * @return 返回 value。
	
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value 要设置的 value。
	
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return 返回 relation。
	
	 */
	public String getRelation() {
		return relation;
	}

	/**
	 * @param relation 要设置的 relation。
	
	 */
	public void setRelation(String relation) {
		this.relation = relation;
	}

	/**
	 * @return 返回 logic。
	
	 */
	public String getLogic() {
		return logic;
	}

	/**
	 * @param logic 要设置的 logic。
	
	 */
	public void setLogic(String logic) {
		this.logic = logic;
	}

	/**
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月14日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "QueryItem [colwhere=" + colwhere + ", value=" + value + ", relation=" + relation + ", logic=" + logic
				+ "]";
	}
	
	
}
