import cn.ccccltd.waf.message.model.vo.MessageSms;
import cn.ccccltd.waf.message.util.CopyMessage;

/**
 * 创建日期:2017年11月13日
 * Title: 测试克隆
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class TestClone {

	
	public static void main(String[] args) {
		
		MessageSms ms = new MessageSms();
		ms.setCommonId("1111111111");
		ms.setPhoneNumber("188");
		MessageSms mscopy = new MessageSms();
		CopyMessage cm = new CopyMessage();
		cm.copyBean(ms, mscopy);
		
		mscopy.setCommonId("2222222222");
		mscopy.setPhoneNumber("99999999");
		System.out.println(ms);
		System.out.println(mscopy);
	}
}
