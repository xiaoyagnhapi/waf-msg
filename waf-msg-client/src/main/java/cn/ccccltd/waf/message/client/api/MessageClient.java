package cn.ccccltd.waf.message.client.api;

import java.util.List;
import java.util.Map;

import cn.ccccltd.waf.message.client.enums.MsgType;
import cn.ccccltd.waf.message.client.vo.QueryMessageEntity;
import cn.ccccltd.waf.message.model.MessageCommon;


public interface MessageClient {

	public Object getMessageResponse(String userId, String moduleId, int pno, int rpp, List<Map<String, Object>> queryitem, MsgType msgType, String systemId);
	
	public Object getEmailMessageResponse(String userId, String moduleId, int pno, int rpp, List<Map<String, Object>> queryitem, String systemId);

	public Object getSmsMessageResponse(String userId, String moduleId, int pno, int rpp, List<Map<String, Object>> queryitem, String systemId);

	public <T extends QueryMessageEntity> Object getDetailMessageResponse(T queryMessageEntity);
	
	public <T extends QueryMessageEntity> Object getStatisticsMessageResponse(T queryMessageEntity);

	public <T extends MessageCommon> Map<String, Object> sendMessage(T smsMessage);

}