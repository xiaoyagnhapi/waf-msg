package cn.ccccltd.waf.message.client.enums;

public enum RelationType {
    
    GE,
    GT,
    EQ,
    LT,
    LE,
    LIKE,
    LLIKE,
    RLIKE

}
