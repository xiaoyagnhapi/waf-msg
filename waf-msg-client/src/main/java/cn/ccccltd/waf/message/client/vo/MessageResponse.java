package cn.ccccltd.waf.message.client.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 列表响应数据对象，用于响应列表数据显示请求。
 * <p>
 * 在列表数据新增、数据删除、数据保存等处理器中会创建此对象并返回处理结果。
 * 
 * @author waf
 */
public class MessageResponse{
	/**
	 * 分页模式下的当前页号
	 */
	private int pno = 1;

	/**
	 * 分页模式下的每页记录数
	 */
	private int rpp = 10;

	/**
	 * 满足查询条件的记录总数。
	 * <p>
	 * 注意分页模式下“满足查询条件的记录总数”和“数据列表的长度”的区别
	 */
	private int total = 0;

	/**
	 * 数据列表
	 */
	private List<Map<String, Object>> rows = null;

	/**
	 * 动态扩充实体vo数据结构
	 * <p>
	 * 该数据结构通过重写service的
	 * {@link WAFService#prepareDynEntityItemData(javax.servlet.http.HttpServletRequest, cn.ccccltd.waf.cache.ListCache, ListResponse)     
	 * 方法进行赋值
	 * 其数据结构为：
     *<pre>
     * {"vo1主键值" : {
     * 		dynCode1 : value,
     * 		dynCode2 : value,
     * 		dynCode3 : value
     * }},
     * {"vo2主键值" : {
     * 		dynCode1 : value,
     * 		dynCode2 : value,
     * 		dynCode3 : value
     * }},
     * .......
	 * </pre>
	 * 前端list组件会自动解析实体项对应的值
	 */
	private Map<String,Map<String,Object>> dynEntityItemsValues = null;


	public MessageResponse() {
		
	}
	
	

	/**
	 * 获取当前页号
	 * 
	 * @return 当前页号
	 */
	public int getPno() {
		return pno;
	}

	/**
	 * 设置当前页号
	 * 
	 * @param pno
	 *            当前页号
	 */
	public void setPno(int pno) {
		this.pno = pno;
	}

	/**
	 * 获取每页记录数
	 * 
	 * @return 每页记录数
	 */
	public int getRpp() {
		return rpp;
	}

	/**
	 * 设置每页记录数
	 * 
	 * @param rpp
	 *            每页记录数
	 */
	public void setRpp(int rpp) {
		this.rpp = rpp;
	}

	/**
	 * 获取满足查询条件的记录总数。
	 * <p>
	 * 注意分页模式下“满足查询条件的记录总数”和“数据列表的长度”的区别
	 * 
	 * @return 满足查询条件的记录总数
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * 设置满足查询条件的记录总数
	 * <p>
	 * 注意分页模式下“满足查询条件的记录总数”和“数据列表的长度”的区别
	 * 
	 * @param total
	 *            满足查询条件的记录总数
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * 获取数据列表
	 * 
	 * @return 数据列表
	 */
	public List<Map<String, Object>> getRows() {
		return rows;
	}

	/**
	 * 设置数据列表
	 * 
	 * @param list
	 *            数据列表
	 */
	public void setRows(List<Map<String, Object>> list) {
		this.rows = list;
	}

	/**
	 * 把VO对象添加到数据列表
	 * 
	 * @param row
	 *            VO对象
	 */
	public  void addToRows(Map<String, Object> row) {
		if (rows == null) {
			rows = new ArrayList<Map<String,Object>>();
		}
		rows.add(row);
	}

	/**
	 * 把VO对象从数据列表中参数
	 * 
	 * @param row
	 *            需删除的VO对象
	 * @return 删除的VO对象
	 */
	public Map<String, Object> deleteFromRows(Map<String, Object> row) {
		if (rows == null) {
			return null;
		}

		for (int i = 0; i < rows.size(); i++) {
			if (rows.get(i) == row) {
				return rows.remove(i);
			}
		}
		return null;
	}

	/**
	 * 获取动态实体的动态实体项数据
	 * <p>
	 * 其数据结构为：
     *<pre>
     * {"vo1主键值" : {
     * 		dynCode1 : value,
     * 		dynCode2 : value,
     * 		dynCode3 : value
     * }},
     * {"vo2主键值" : {
     * 		dynCode1 : value,
     * 		dynCode2 : value,
     * 		dynCode3 : value
     * }},
     * .......
	 * </pre>
	 * 
	 * @return 动态实体项数据
	 */
	public Map<String, Map<String, Object>> getDynEntityItemsValues() {
		if (dynEntityItemsValues == null) {
			dynEntityItemsValues = new HashMap<String,Map<String,Object>>();
		}
		
		return dynEntityItemsValues;
	}

	/**
	 * 设置动态实体的动态实体项数据
	 * <p>
	 * 其数据结构为：
     *<pre>
     * {"vo1主键值" : {
     * 		dynCode1 : value,
     * 		dynCode2 : value,
     * 		dynCode3 : value
     * }},
     * {"vo2主键值" : {
     * 		dynCode1 : value,
     * 		dynCode2 : value,
     * 		dynCode3 : value
     * }},
     * .......
	 * </pre>
	 * @param
	 *       动态实体项数据
	 */
	public void setDynEntityItemsValues(Map<String, Map<String, Object>> dynEntityItemsValues) {
		this.dynEntityItemsValues = dynEntityItemsValues;
	}
	
	/**
	 * 添加动态实体的实体项数据
	 * <p>
	 * 其数据结构为：
     *<pre>
     * {"vo1主键值" : {
     * 		dynCode1 : value,
     * 		dynCode2 : value,
     * 		dynCode3 : value
     * }},
     * {"vo2主键值" : {
     * 		dynCode1 : value,
     * 		dynCode2 : value,
     * 		dynCode3 : value
     * }},
     * .......
	 * </pre>
	 * 
	 * @param key
	 *          vo主键值
	 * @param map
	 *          map<实体项编码， 实体项编码对应的值>
	 */
	public void putToDynEntityItemsValues(String key, Map<String, Object> map) {
		if (dynEntityItemsValues == null) {
			dynEntityItemsValues = new HashMap<String,Map<String,Object>>();
		}

		dynEntityItemsValues.put(key, map);
	}

	/**
	 * 清空数据结构
	 */
	public void clearDynEntityItemsValuesMap() {
		if (dynEntityItemsValues != null) {
			dynEntityItemsValues.clear();
		}
	}
}
