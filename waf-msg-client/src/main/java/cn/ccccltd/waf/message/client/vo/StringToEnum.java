package cn.ccccltd.waf.message.client.vo;

import cn.ccccltd.waf.message.client.enums.MsgType;
import cn.ccccltd.waf.message.client.enums.OrderRule;
import cn.ccccltd.waf.message.client.enums.RelationType;

public class StringToEnum {

	public static MsgType msgType;
	public static OrderRule orderRule;
	public static RelationType relationType;
	
	public static MsgType StringToMsgType(String str){
		str = str.toUpperCase();
		switch (str) {
		case "SMS":
			msgType = MsgType.SMS;
			break;
		case "EMAIL":
			msgType = MsgType.EMAIL;
			break;
		default:
			return null;
		}
		return msgType;
	}
	
	public static OrderRule StringToOrderRule(String str){
		str = str.toUpperCase();
		switch (str) {
		case "DESC":
			orderRule = OrderRule.DESC;
			break;
		case "ASC":
			orderRule = OrderRule.ASC;
			break;
		default:
			return null;
		}
		return orderRule;
	}
	
	public static RelationType StringToRelationType(String str){
		str = str.toUpperCase();
		switch (str) {
		case "GE":
			relationType = RelationType.GE;
			break;
		case "GT":
			relationType = RelationType.GT;
			break;
		case "EQ":
			relationType = RelationType.EQ;
			break;
		case "LT":
			relationType = RelationType.LT;
			break;
		case "LE":
			relationType = RelationType.LE;
			break;
		case "LIKE":
			relationType = RelationType.LIKE;
			break;
		case "LLIKE":
			relationType = RelationType.LLIKE;
			break;
		case "RLIKE":
			relationType = RelationType.RLIKE;
			break;
		default:
			return null;
		}
		return relationType;
	}
	
}
