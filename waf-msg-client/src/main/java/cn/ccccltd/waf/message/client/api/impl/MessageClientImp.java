package cn.ccccltd.waf.message.client.api.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import cn.ccccltd.waf.message.client.api.MessageClient;
import cn.ccccltd.waf.message.client.enums.MsgType;
import cn.ccccltd.waf.message.client.vo.MessageResponse;
import cn.ccccltd.waf.message.client.vo.QueryMessageEntity;
import cn.ccccltd.waf.message.model.MessageCommon;
import cn.ccccltd.waf.message.util.JsonUtils;
import cn.ccccltd.waf.message.util.NameUtil;

@Component
public class MessageClientImp implements MessageClient{

	@Override
	public MessageResponse getEmailMessageResponse(String sendUserid, String businessModule, int pno, int rpp, List<Map<String, Object>> queryitem,
			 String sendSystemid) {
		return getMessageResponse(sendUserid, businessModule, pno, rpp, queryitem, MsgType.EMAIL, sendSystemid);
	}
	
	@Override
	public MessageResponse getSmsMessageResponse(String sendUserid, String businessModule, int pno, int rpp, List<Map<String, Object>> queryitem,
			String sendSystemid) {
		return getMessageResponse(sendUserid, businessModule, pno, rpp, queryitem, MsgType.SMS, sendSystemid);
	}
	
	@Override
	public MessageResponse getMessageResponse(String sendUserid, String businessModule, int pno, int rpp, List<Map<String, Object>> queryitem,
			MsgType msgType, String sendSystemid) {
		// TODO Auto-generated method stub
		String category = null;
		switch ( msgType ) {
        case EMAIL:
        	category = "email";
            break;
        case SMS :
        	category = "sms";
            break;
        default:
            break;
        }
		
		Map<String , Integer> page = new HashMap<String , Integer>(); 
		page.put("pno", 1);
		page.put("rpp", 10);
		
		
		
//		String url = "http://192.168.20.126/waf-message/message?secret=123";
		
		Map<String , Object> map = new HashMap<String , Object>(); 
		
		map.put("sendUserid", sendUserid);
		map.put("businessModule", businessModule);
//		map.put("pno", pno);
//		map.put("rpp", rpp);
		map.put("page", page);
		map.put("order", "order");
		map.put("queryitem", queryitem);
		map.put("category", category);
		map.put("sendSystemid", "123");
		
		String json = JsonUtils.toJson(map);
		
        //get
        String url = "http://127.0.0.1:8080/mq/waf/msg/meta/getdata.bo?json={json}&sendSystemid={sendSystemid}";
//        String url = "http://192.168.20.126/waf-message/message/msgList?json={json}&sendSystemid={sendSystemid}";
//		String url = "http://192.168.20.126/waf-message/message?secret=123";
        
        RestTemplate restTemplate = new RestTemplate();
        
        Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("json", json);
        uriVariables.put("sendSystemid", "123");
        String result = restTemplate.getForObject(url, String.class, uriVariables);
        
        result = NameUtil.getUnderLineName(result);
        
        Map<String,Object> resultMap = JsonUtils.toObject(result,Map.class);
        
        List<Map<String, Object>> list = (List<Map<String, Object>>) resultMap.get("datas");
        
        int total = (int) resultMap.get("total");
        
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setRows(list);
        messageResponse.setTotal(total);
        
		return messageResponse;
	}

	@Override
	public MessageResponse getDetailMessageResponse(QueryMessageEntity queryMessageEntity) {
		
		String json = JsonUtils.toJson(queryMessageEntity);
		String urlStr = json;
//		try {
//			urlStr = URLEncoder.encode(json, "utf-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
        String url = "http://192.168.20.126/waf-message/message/msgList?json={json}&sendSystemid={sendSystemid}";
        RestTemplate restTemplate = new RestTemplate();
        Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("json", urlStr);
        uriVariables.put("sendSystemid", "1111");
        String result = restTemplate.getForObject(url, String.class, uriVariables);
        
        result = NameUtil.getUnderLineName(result);
        
        Map<String,Object> resultMap = JsonUtils.toObject(result,Map.class);
        
        List<Map<String, Object>> list = new LinkedList<Map<String, Object>>();
        
        Map<String, Object> dataMap = (Map<String, Object>) resultMap.get("data");
        
        int total = (int) dataMap.get("total");
        
        list = (List<Map<String, Object>>) dataMap.get("rows");
        
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setRows(list);
        messageResponse.setTotal(total);
        
        Map<String, Integer> page = queryMessageEntity.getPage();
        if (null != page) {
        	messageResponse.setPno(page.get("pno"));
			messageResponse.setRpp(page.get("rpp"));
        }
		return messageResponse;
	}

	@Override
	public MessageResponse getStatisticsMessageResponse(QueryMessageEntity queryMessageEntity) {
		
		String json = JsonUtils.toJson(queryMessageEntity);
		String urlStr = json;
//		try {
//			urlStr = URLEncoder.encode(json, "utf-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		String url = "http://192.168.20.126/waf-message/message/msgStat?json={json}&sendSystemid={sendSystemid}";
		RestTemplate restTemplate = new RestTemplate();
		Map<String, Object> uriVariables = new HashMap<String, Object>();
		uriVariables.put("json", urlStr);
		uriVariables.put("sendSystemid", "1111");
		String result = restTemplate.getForObject(url, String.class, uriVariables);
		
		result = NameUtil.getUnderLineName(result);
		
		Map<String,Object> resultMap = JsonUtils.toObject(result,Map.class);
		
		List<Map<String, Object>> list = new LinkedList<Map<String, Object>>();
		
		Map<String, Object> dataMap = (Map<String, Object>) resultMap.get("data");
		
		int total = (int) dataMap.get("total");
		
		list = (List<Map<String, Object>>) dataMap.get("rows");
		
		MessageResponse messageResponse = new MessageResponse();
		messageResponse.setRows(list);
		messageResponse.setTotal(total);
		
		Map<String, Integer> page = queryMessageEntity.getPage();
		if (null != page) {
			messageResponse.setPno(page.get("pno"));
			messageResponse.setRpp(page.get("rpp"));
		}
		return messageResponse;
	}

	@Override
	public Map<String, Object> sendMessage(MessageCommon smsMessage) {
		// TODO Auto-generated method stub
		
		String json = JsonUtils.toJson(smsMessage);
		String urlStr = json;
//		try {
//			urlStr = URLEncoder.encode(json, "utf-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
        String url = "http://192.168.20.126/waf-message/message?sendSystemid={sendSystemid}";
        RestTemplate restTemplate = new RestTemplate();
        
        HttpHeaders headers = new HttpHeaders();  
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);  
        HttpEntity<String> requestEntity = new HttpEntity<String>(json,  headers);  
        
        Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("sendSystemid", "1111");
        String result = restTemplate.postForObject(url, requestEntity, String.class, uriVariables);
        
        result = NameUtil.getUnderLineName(result);
        
        Map<String,Object> resultMap = JsonUtils.toObject(result,Map.class);
//        
        String message = (String) resultMap.get("msg");
        String success = (String) resultMap.get("success");
        String stateCode = (String) resultMap.get("stateCode");
        
		return resultMap;
	}

}

//无法指定编码格式  post
//MultiValueMap<String, Object> entity = new LinkedMultiValueMap<>();
//entity.add("json", json);
//ResponseEntity<String> resp = restTemplate.postForEntity(url,entity,String.class);
//String lres = resp.getBody();
