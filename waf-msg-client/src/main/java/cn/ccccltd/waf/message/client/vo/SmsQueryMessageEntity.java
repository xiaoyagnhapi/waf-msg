package cn.ccccltd.waf.message.client.vo;

import java.util.HashMap;
import java.util.Map;

import cn.ccccltd.waf.message.client.enums.RelationType;

public class SmsQueryMessageEntity extends QueryMessageEntity{

	public void setDeliveredTime(String value, RelationType relation, String logic) {
		Map<String, String> map = new HashMap<>(4);
		map.put("colwhere", "delivered_time");
		map.put("relation", relation.toString());
		map.put("value", value);
		map.put("logic", logic);
		queryItems.add(map);
	}
	public void setSendState(String value, RelationType relation, String logic) {
		Map<String, String> map = new HashMap<>(4);
		map.put("colwhere", "send_state");
		map.put("relation", relation.toString());
		map.put("value", value);
		map.put("logic", logic);
		queryItems.add(map);
	}
	public void setSendTimes(String value, RelationType relation, String logic) {
		Map<String, String> map = new HashMap<>(4);
		map.put("colwhere", "send_times");
		map.put("relation", relation.toString());
		map.put("value", value);
		map.put("logic", logic);
		queryItems.add(map);
	}
	public void setPhoneNumber(String value, RelationType relation, String logic) {
		Map<String, String> map = new HashMap<>(4);
		map.put("colwhere", "phone_number");
		map.put("relation", relation.toString());
		map.put("value", value);
		map.put("logic", logic);
		queryItems.add(map);
	}
	public void setSendSystemid(String value, RelationType relation, String logic) {
		Map<String, String> map = new HashMap<>(4);
		map.put("colwhere", "send_systemid");
		map.put("relation", relation.toString());
		map.put("value", value);
		map.put("logic", logic);
		queryItems.add(map);
	}
	public void setBusinessModule(String value, RelationType relation, String logic) {
		Map<String, String> map = new HashMap<>(4);
		map.put("colwhere", "business_module");
		map.put("relation", relation.toString());
		map.put("value", value);
		map.put("logic", logic);
		queryItems.add(map);
	}
	public void setSendUserid(String value, RelationType relation, String logic) {
		Map<String, String> map = new HashMap<>(4);
		map.put("colwhere", "send_userid");
		map.put("relation", relation.toString());
		map.put("value", value);
		map.put("logic", logic);
		queryItems.add(map);
	}
	public void setCommonId(String value, RelationType relation, String logic) {
		Map<String, String> map = new HashMap<>(4);
		map.put("colwhere", "common_id");
		map.put("relation", relation.toString());
		map.put("value", value);
		map.put("logic", logic);
		queryItems.add(map);
	}
	public void setCategory(String value, RelationType relation, String logic) {
		Map<String, String> map = new HashMap<>(4);
		map.put("colwhere", "category");
		map.put("relation", relation.toString());
		map.put("value", value);
		map.put("logic", logic);
		queryItems.add(map);
	}
	public void setSendTime(String value, RelationType relation, String logic) {
		Map<String, String> map = new HashMap<>(4);
		map.put("colwhere", "send_time");
		map.put("relation", relation.toString());
		map.put("value", value);
		map.put("logic", logic);
		queryItems.add(map);
	}
	
}
