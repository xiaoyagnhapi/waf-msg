package cn.ccccltd.waf.message.client.vo;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cn.ccccltd.waf.message.client.enums.OrderRule;

public class QueryMessageEntity {

	private String sendUserid;
	private String sendSystemid;
	private String businessModule;
	private String category = "sms";

	private Map<String, Integer> page = new HashMap<>(2);
	
	protected List<Map<String, String>> queryItems = new LinkedList<>();
	
	private List<Map<String, String>> orders = new LinkedList<>();

	public String getCategory() {
		return category;
	}

	public QueryMessageEntity setCategory(String category) {
		this.category = category;
		return this;
	}

	public String getSendUserid() {
		return sendUserid;
	}

	public void setSendUserid(String sendUserid) {
		this.sendUserid = sendUserid;
	}

	public String getSendSystemid() {
		return sendSystemid;
	}

	public void setSendSystemid(String sendSystemid) {
		this.sendSystemid = sendSystemid;
	}

	public String getBusinessModule() {
		return businessModule;
	}

	public void setBusinessModule(String businessModule) {
		this.businessModule = businessModule;
	}

	public Map<String, Integer> getPage() {
		return page;
	}

	public void setPage(Map<String, Integer> page) {
		this.page = page;
	}
	
	public void setPagePno(int pno) {
		this.page.put("pno", pno);
	}

	public void setPageRpp(int rpp) {
		this.page.put("rpp", rpp);
	}

	public List<Map<String, String>> getQueryItems() {
		return queryItems;
	}

	public void setQueryItems(List<Map<String, String>> queryitems) {
		this.queryItems = queryitems;
	}
	
	public List<Map<String, String>> getOrders() {
		return orders;
	}

	public void setOrders(List<Map<String, String>> orders) {
		this.orders = orders;
	}

	public void putOrder(String sortName,String sortType) {
		Map<String, String> map = new HashMap<>(2);
		map.put("sortName", sortName);
		map.put("sortType", sortType);
		orders.add(map);
	}
	
	public void setCommonId(OrderRule orderrule) {
		Map<String, String> map = new HashMap<>(2);
		map.put("sortName", "common_id");
		map.put("sortType", orderrule.toString());
		orders.add(map);
	}
	
	public void setPhoneNumber(OrderRule orderrule) {
		Map<String, String> map = new HashMap<>(2);
		map.put("sortName", "phone_number");
		map.put("sortType", orderrule.toString());
		orders.add(map);
	}
	public void setSendTimes(OrderRule orderrule) {
		Map<String, String> map = new HashMap<>(2);
		map.put("sortName", "send_times");
		map.put("sortType", orderrule.toString());
		orders.add(map);
	}
	public void setSendState(OrderRule orderrule) {
		Map<String, String> map = new HashMap<>(2);
		map.put("sortName", "send_state");
		map.put("sortType", orderrule.toString());
		orders.add(map);
	}
	
	public void setDeliveredTime(OrderRule orderrule) {
		Map<String, String> map = new HashMap<>(2);
		map.put("sortName", "delivered_time");
		map.put("sortType", orderrule.toString());
		orders.add(map);
	}
}

