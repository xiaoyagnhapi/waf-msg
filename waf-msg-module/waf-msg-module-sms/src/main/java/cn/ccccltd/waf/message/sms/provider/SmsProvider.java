package cn.ccccltd.waf.message.sms.provider;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.alibaba.fastjson.JSONObject;

import cn.ccccltd.waf.message.base.provider.BaseMessageService;
import cn.ccccltd.waf.message.model.result.MessageResult;
import cn.ccccltd.waf.message.model.vo.MessageSms;
import cn.ccccltd.waf.message.util.CopyMessage;
import cn.ccccltd.waf.message.util.DateUtilsNew;
import cn.ccccltd.waf.message.util.JsonUtils;
import cn.ccccltd.waf.message.util.ToolUtils;

/**
 * 创建日期:2017年10月31日
 * Title:生产者
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@SuppressWarnings("rawtypes")
@Component("sms")
public class SmsProvider extends BaseMessageService{

	/**JmsTemplate必须要用resource,且固定注入名称*/
	@Resource(name="smsJmsTemplate")
	private JmsTemplate jmsTemplate;
	
	/**单线程处理的最大数*/
    @Value("${sms.group.size}")
    private int groupSize;
	
	@Override
	protected String getTableName() {
		return "waf_msg_sms";
	}

	@Override
	protected Class getEntityClass() {
		return MessageSms.class;
	}
	
	@Override
	protected JmsTemplate getJmsTemplate() {
		return jmsTemplate;
	}
	
	/**
	 * 功能: 消息发送队列及保存消息表<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月1日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param content
	 * @return
	 * @see cn.ccccltd.waf.message.base.provider.BaseMessageService#sendMessage(java.lang.String)
	 */
	@SuppressWarnings({ "unchecked"})
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Object sendMessage(JSONObject content) {
		
		MessageResult result = new MessageResult();
		
		MessageSms smsMessage = JsonUtils.toObject(content.toJSONString(), MessageSms.class);
		
		Assert.hasText(smsMessage.getMsgTopic(),"短信内容不能为空！");
		
		//如果发送时间没有或者是null进行相应填值处理
		if(StringUtils.isBlank(smsMessage.getSendTime())) {
			smsMessage.setSendTime(DateUtilsNew.getDateTime());
		}
		
		//消息队列消息统一发送
		List<MessageSms> msgListQueue = new ArrayList<>();
		
		//插入数据到公共表
		insertMessageSuperTable(smsMessage);
		//将消息父id放到关联common_id上
		smsMessage.setCommonId(smsMessage.getId());
		
		//消息的创建时间
		smsMessage.setCreateTime(DateUtilsNew.getDateTime());
		
		String phoneNumbers = smsMessage.getPhoneNumber().trim();
		
		Assert.hasText(phoneNumbers,"手机号不能为空！");
		
		//插入消息
		if (phoneNumbers.contains(",")) {
			CopyMessage cm = new CopyMessage();
			for (String phoneNumber : phoneNumbers.split(",")) {
				
				Assert.isTrue(ToolUtils.isPhone(phoneNumber), phoneNumber + "手机号码验证错误！");
				
				MessageSms wms = new MessageSms();
				//cglib拷贝对象
				cm.copyBean(smsMessage, wms);
				wms.setSmsId(ToolUtils.getUUID());
				wms.setPhoneNumber(phoneNumber);
				//插入数据到具体消息表
				insertMessageTable(wms);
				//加入到队列待发送集合中
				msgListQueue.add(wms);
			}
			
		} else {
			
			Assert.isTrue(ToolUtils.isPhone(phoneNumbers), phoneNumbers + "手机号码验证错误！");
			
			smsMessage.setPhoneNumber(phoneNumbers);
			//插入数据到具体消息表
			insertMessageTable(smsMessage);
			msgListQueue.add(smsMessage);
			
		}
		
		//发送消息到消息队列
		if (msgListQueue.size()>0) {
			//队列根据基数groupSize来判断是否分线程处理发送消息
			return sendMessageByThread(msgListQueue, groupSize);
			
		}
		result.setSuccess("false");
		result.setStateCode("500");
		result.setMsg("队列发送失败，请联系管理员！");
		
		return result;
	}

}
