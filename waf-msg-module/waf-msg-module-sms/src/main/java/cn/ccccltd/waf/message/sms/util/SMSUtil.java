package cn.ccccltd.waf.message.sms.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SMSUtil {
	private static Logger log = LoggerFactory.getLogger(SMSUtil.class);
	private static String httpControllerUrl;
	private static String syscode;
	private static String syspwd;
	static{
		InputStream is = SMSUtil.class.getResourceAsStream("/META-INF/config/sms.properties");
		if(is!=null){
			Properties prop = new Properties();
			try {
				prop.load(is);
				httpControllerUrl=prop.getProperty("sms.httpcontrollerurl");
				syscode=prop.getProperty("sms.syscode");
				syspwd=prop.getProperty("sms.syspwd");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 发送单条或多条短信，短信号码以逗号隔开
	 * @param receiverphones
	 * @param content
	 * @return
	 */
	public static boolean sendSms(String receiverphones,String content){
		CloseableHttpClient httpclient = HttpClients.createDefault();
		String result="";
		try {
			HttpPost httpPost = new HttpPost(httpControllerUrl);
			List <NameValuePair> nvps = new ArrayList <NameValuePair>();
			nvps.add(new BasicNameValuePair("receiverphones",receiverphones));
			nvps.add(new BasicNameValuePair("syscode",syscode));
			nvps.add(new BasicNameValuePair("syspwd", syspwd));
			nvps.add(new BasicNameValuePair("content",content));
			nvps.add(new BasicNameValuePair("multisend","true"));
			httpPost.setEntity(new UrlEncodedFormEntity(nvps,"UTF-8"));
			CloseableHttpResponse response = httpclient.execute(httpPost);
		    System.out.println(response.getStatusLine());
		    HttpEntity entity = response.getEntity();
		    result=EntityUtils.toString(entity);
		    response.close();
		}catch(ClientProtocolException ex){
			log.error(ex.getLocalizedMessage());
			ex.printStackTrace();
		}catch(IOException e){
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
		}finally {
		    try {
				httpclient.close();
			} catch (IOException e) {
				log.error(e.getLocalizedMessage());
				e.printStackTrace();
			}
		}
		if("true".equals(result)){
			return true;
		}else{
			return false;
		}
	}
	/**
	 * 带操作者详细信息发送单条短信
	 * @param operid
	 * @param receiveruserid
	 * @param receiveraccount
	 * @param receivername
	 * @param receiverphone
	 * @param sendtime
	 * @param content
	 * @return
	 */
	public static boolean sendSms(String operid,String receiveruserid, String receiveraccount,String receivername,String receiverphone,String sendtime,String content){
		CloseableHttpClient httpclient = HttpClients.createDefault();
		String result="";
		try {
			HttpPost httpPost = new HttpPost(httpControllerUrl);
			List <NameValuePair> nvps = new ArrayList <NameValuePair>();
			nvps.add(new BasicNameValuePair("operid",operid));
			nvps.add(new BasicNameValuePair("receiveruserid",receiveruserid));
			nvps.add(new BasicNameValuePair("receiveraccount",receiveraccount));
			nvps.add(new BasicNameValuePair("receivername",receivername));
			nvps.add(new BasicNameValuePair("receiverphone",receiverphone));
			nvps.add(new BasicNameValuePair("sendtime",sendtime));
			nvps.add(new BasicNameValuePair("syscode",syscode));
			nvps.add(new BasicNameValuePair("syspwd", syspwd));
			nvps.add(new BasicNameValuePair("content",content));
	
			httpPost.setEntity(new UrlEncodedFormEntity(nvps,"UTF-8"));
			CloseableHttpResponse response = httpclient.execute(httpPost);
		    System.out.println(response.getStatusLine());
		    HttpEntity entity = response.getEntity();
		    result=EntityUtils.toString(entity);
		    response.close();
		}catch(ClientProtocolException ex){
			log.error(ex.getLocalizedMessage());
			ex.printStackTrace();
		}catch(IOException e){
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
		}finally {
		    try {
				httpclient.close();
			} catch (IOException e) {
				log.error(e.getLocalizedMessage());
				e.printStackTrace();
			}
		}
		if("true".equals(result)){
			return true;
		}else{
			return false;
		}
	}

	
	public static void main(String[] args){
		//SMSUtil.sendSms("operid", "receiveruserid", "receiveraccount", "receivername", "13910326704", null, "内容");
		SMSUtil.sendSms("13910326704", "内容1");
		//SMSUtil.sendSms("13871130197,13618683111,13907259998,13339726118,15972964203,13986763242,13971703229,13367210006,18972618148,13554375078,13466541371", "据气象台预报：未来三天，我省大部分地区将出现低温雨雪天气，气温降至0℃以下，并伴有较明显冰冻现象。各地要及时将天气情况通报至各涉水乡镇政府、航运企业及船员，注意防范，特别是上下船坡道和甲板要及时铺设防滑草垫、渡运时跳板要上翘、渡运车辆要加垫三角木等安全措施，恶劣天气时加强现场监管和应急值班，严格落实限航停航措施。");
		//SMSUtil.sendSms("13871130197,13618683111,13907259998,13339726118,15972964203,13986763242,13971703229,13367210006,18071058209,13554375078,13466541371", "此次雨雪低温极端天气属几十年不遇。根据局领导指示，各地需按最高级别启动水上安全预警，切实加强组织领导，基层海事人员务必全部出动到现场盯防，市局要派暗访组进行暗访，并将落实情况于21号上午下班前反馈省局安全监督处。省局将派两个暗访组赴现场暗访。");
	}
}
