package cn.ccccltd.waf.message.sms.config;

import javax.jms.ConnectionFactory;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;

import cn.ccccltd.waf.message.sms.constant.SmsConstants;

/**
 * 创建日期:2017年11月3日
 * Title:短信配置信息
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Configuration
@EnableJms
public class SmsQueueConfig {
	
	@Autowired
	private ConnectionFactory connectionFactory;
	
	@Bean(name  = "smsQueue")
	public ActiveMQQueue smsQueue() {
		return new ActiveMQQueue(SmsConstants.SMS_QUEUE);
		
	}
	/**
	 * 功能:短信队列模板
	 * 作者: yangjingjiang
	 * 创建日期:2017年10月31日
	 * 修改者: mender
	 * 修改日期: modifydate
	 * @return
	 */
	@Bean(name = "smsJmsTemplate")
	public JmsTemplate smsJmsTemplate() {
		
		JmsTemplate jmsTemplate = new JmsTemplate();
		
		jmsTemplate.setConnectionFactory(connectionFactory);
		jmsTemplate.setDefaultDestinationName(SmsConstants.SMS_QUEUE);

		return jmsTemplate;
		
	}
	/**
	 * 功能: 短信表更新状态队列<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean(name  = "smsUpdateTableQueue")
	public ActiveMQQueue smsUpdateTableQueue() {
		return new ActiveMQQueue(SmsConstants.SMS_UPDATETABLE_QUEUE);
		
	}
	
	/**
	 * 功能:短信表更新状态模板
	 * 作者: yangjingjiang
	 * 创建日期:2017年10月31日
	 * 修改者: mender
	 * 修改日期: modifydate
	 * @return
	 */
	@Bean(name = "smsUpdateTableJmsTemplate")
	public JmsTemplate smsUpdateTableJmsTemplate() {
		
		JmsTemplate jmsTemplate = new JmsTemplate();
		
		jmsTemplate.setConnectionFactory(connectionFactory);
		jmsTemplate.setDefaultDestinationName(SmsConstants.SMS_UPDATETABLE_QUEUE);
		
		return jmsTemplate;
		
	}
	
}
