package cn.ccccltd.waf.message.sms.consumer;

import javax.annotation.Resource;
import javax.jms.Message;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;

import cn.ccccltd.waf.message.base.consumer.BaseUpdateTableConsumer;
import cn.ccccltd.waf.message.model.vo.MessageSms;
import cn.ccccltd.waf.message.sms.constant.SmsConstants;
import cn.ccccltd.waf.message.util.JsonUtils;
import cn.ccccltd.waf.message.util.sql.SqlContext;
import cn.ccccltd.waf.message.util.sql.SqlTools;

/**
 * 创建日期:2017年10月31日
 * Title:短息处理的类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Component("smsUpdateTableConsumer")
public class SmsUpdateTableConsumer extends BaseUpdateTableConsumer{

	@Resource(name="smsUpdateTableJmsTemplate")
	private JmsTemplate smsUpdateTableJmsTemplate;

	@Override
	protected SqlContext getUpdateSqlContext(JSONObject smsMessage) {
		MessageSms sms = JsonUtils.toObject(smsMessage.toJSONString(), MessageSms.class);
		SqlContext updateContext = SqlTools.getUpdate(sms);
		
		return updateContext;
	}

	@Override
	protected JmsTemplate getUpdateTableJmsTemplate() {
		
		return smsUpdateTableJmsTemplate;
		
	}

	/**
	 * 功能: 短信处理db队列 <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param message
	 * @see cn.ccccltd.waf.message.base.consumer.ConsumerService#receiveMessage(javax.jms.Message)
	 */
	@JmsListener(destination=SmsConstants.SMS_UPDATETABLE_QUEUE, containerFactory="jmsListenerContainerFactory" )
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void receiveMessage(Message message) {
		
		super.receiveMessage(message);
		
	}

	
}
