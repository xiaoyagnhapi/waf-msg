package cn.ccccltd.waf.message.sms.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import cn.ccccltd.waf.message.config.PropertiesConfig;

/**
 * 创建日期:2017年11月17日
 * Title: 通过spring读取配置的类注：不能添加其他内容，使用时用	Value注解("${mail.host}")
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Configuration
@PropertySource(value = {"/META-INF/config/sms.properties"})
public class SmsPropertiesConfig extends PropertiesConfig{

}