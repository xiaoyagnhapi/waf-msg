package cn.ccccltd.waf.message.sms.consumer;

import java.util.Map;

import javax.annotation.Resource;
import javax.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;

import cn.ccccltd.waf.message.base.consumer.BaseConsumer;
import cn.ccccltd.waf.message.queue.log.util.LogMessageUtils;
import cn.ccccltd.waf.message.sms.constant.SmsConstants;
import cn.ccccltd.waf.message.sms.util.SMSUtil;

/**
 * 创建日期:2017年10月31日
 * Title:短息处理的类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Component("smsConsumer")
public class SmsConsumer extends BaseConsumer{

	private static Logger log = LoggerFactory.getLogger(SmsConsumer.class);
	
	@Resource(name="smsUpdateTableJmsTemplate")
	private JmsTemplate smsUpdateTableJmsTemplate;
	
	
	/**
	 * 功能:短信的消费者 
	 * 作者: yangjingjiang
	 * 创建日期:2017年10月31日
	 * 修改者: mender
	 * 修改日期: modifydate
	 * @param msg
	 * @see cn.ccccltd.waf.message.base.consumer.ConsumerService#receiveMessage(java.lang.String)
	 */
	@JmsListener(destination=SmsConstants.SMS_QUEUE, containerFactory="jmsListenerContainerFactory" )
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void receiveMessage(Message message) {
		
		super.receiveMessage(message);
		
	}
	

	@Override
	protected Map<String, String> sendMessage(JSONObject smsMessage) {

		String code = "200";
		String message = "短信发送成功！";
		/**
		 * 具体的发送消息的
		 */
		try {
			
			
			SMSUtil.sendSms(smsMessage.getString("phoneNumber"), smsMessage.getString("msgTopic"));
			
		} catch (Exception e) {
			
			log.error("cn.ccccltd.waf.message.sms.consumer.SmsConsumer.sendMessage(JSONObject)，短信发送失败！");
			code = "500";
			message = e.getLocalizedMessage();
			e.printStackTrace();
		}
		/**
		 * smsMessage.getString("smsId") 短信邮件等真实表的id
		 */
		return LogMessageUtils.getSendLogMap(smsMessage.getString("smsId"), code, message);
	}

	@Override
	protected JmsTemplate getJmsTemplate() {
		
		return smsUpdateTableJmsTemplate;
		
	}
	
}
