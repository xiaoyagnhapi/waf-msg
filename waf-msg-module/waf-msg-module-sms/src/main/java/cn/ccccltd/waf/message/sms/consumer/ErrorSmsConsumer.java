package cn.ccccltd.waf.message.sms.consumer;

import java.util.Map;

import javax.annotation.Resource;
import javax.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

import cn.ccccltd.waf.message.base.consumer.BaseErrorConsumer;
import cn.ccccltd.waf.message.constant.MessageConstants;
import cn.ccccltd.waf.message.model.vo.MessageSms;
import cn.ccccltd.waf.message.queue.error.constant.ErrorConstants;
import cn.ccccltd.waf.message.queue.log.util.LogMessageUtils;
import cn.ccccltd.waf.message.sms.constant.SmsConstants;
import cn.ccccltd.waf.message.sms.util.SMSUtil;
import cn.ccccltd.waf.message.util.JsonUtils;
import cn.ccccltd.waf.message.util.sql.SqlContext;
import cn.ccccltd.waf.message.util.sql.SqlTools;

/**
 * 创建日期:2017年11月3日
 * Title: 发送短信失败重试消费者
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Component("errorSmsConsumer")
public class ErrorSmsConsumer extends BaseErrorConsumer{

	private static Logger log = LoggerFactory.getLogger(ErrorSmsConsumer.class);
	
	@Resource(name="smsUpdateTableJmsTemplate")
	private JmsTemplate smsUpdateTableJmsTemplate;
	
	@Override
	@JmsListener(destination=ErrorConstants.ERROR_QUEUE, selector = MessageConstants.SELECTOR + "='"+SmsConstants.SMS_ERROR_MESSAGE_SELECTOR+"'", containerFactory="jmsListenerContainerFactory" )
	public void receiveMessage(Message message) {
		
		super.receiveMessage(message);
		
	}

	@Override
	protected SqlContext getUpdateSqlContext(JSONObject smsMessage) {
		
		MessageSms sms = JsonUtils.toObject(smsMessage.toJSONString(), MessageSms.class);
		SqlContext updateContext = SqlTools.getUpdate(sms);
		
		return updateContext;
	}

	@Override
	protected JmsTemplate getUpdateTableJmsTemplate() {
		
		return smsUpdateTableJmsTemplate;
		
	}

	@Override
	protected Map<String, String> sendMessage(JSONObject smsMessage) {

		String code = "200";
		String message = "短信发送成功！";
		/**
		 * 具体的发送消息的
		 */
		try {
			
			
			SMSUtil.sendSms(smsMessage.getString("phoneNumber"), smsMessage.getString("msgTopic"));
			
		} catch (Exception e) {
			
			log.error("cn.ccccltd.waf.message.sms.consumer.SmsConsumer.sendMessage(JSONObject)，短信发送失败！");
			code = "500";
			message = e.getLocalizedMessage();
			e.printStackTrace();
		}
		/**
		 * smsMessage.getString("smsId") 短信邮件等真实表的id
		 */
		return LogMessageUtils.getSendLogMap(smsMessage.getString("smsId"), code, message);
	}
	
	
}
