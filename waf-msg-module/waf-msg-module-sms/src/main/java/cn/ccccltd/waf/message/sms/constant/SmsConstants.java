package cn.ccccltd.waf.message.sms.constant;

/**
 * 创建日期:2017年11月2日
 * Title: 定义队列名称
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class SmsConstants {

	/**短信队列的名称*/
	public static final String SMS_QUEUE = "waf.message.queue.sms";
	/**短信错误信息过滤条件 插件名称+ErrorMessage*/
	public static final String SMS_ERROR_MESSAGE_SELECTOR = "smsErrorMessage";
	/**短信更新状态队列*/
	public static final String SMS_UPDATETABLE_QUEUE = "waf.message.queue.sms.table";

}
