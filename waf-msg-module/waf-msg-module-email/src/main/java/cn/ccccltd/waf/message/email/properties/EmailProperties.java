package cn.ccccltd.waf.message.email.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 创建日期:2017年11月17日
 * Title: 邮箱的配置类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Component
public class EmailProperties {
	
	@Value("${mail.host}")
	private String host;
	@Value("${mail.username}")
	private String username;
	@Value("${mail.password}")
	private String password;
	@Value("${mail.smtp.auth}")
	private String auth;
	@Value("${mail.smtp.timeout}")
	private String timeout;
	@Value("${mail.default.from}")
	private String from;
	@Value("${mail.group.size}")
	private int groupSize;
	
	public EmailProperties() {
		super();
	}

	public EmailProperties(String host, String username, String password, String auth, String timeout, String from,
			int groupSize) {
		super();
		this.host = host;
		this.username = username;
		this.password = password;
		this.auth = auth;
		this.timeout = timeout;
		this.from = from;
		this.groupSize = groupSize;
	}

	/**
	 * @return 返回 host。
	
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host 要设置的 host。
	
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return 返回 username。
	
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username 要设置的 username。
	
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return 返回 password。
	
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password 要设置的 password。
	
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return 返回 auth。
	
	 */
	public String getAuth() {
		return auth;
	}

	/**
	 * @param auth 要设置的 auth。
	
	 */
	public void setAuth(String auth) {
		this.auth = auth;
	}

	/**
	 * @return 返回 timeout。
	
	 */
	public String getTimeout() {
		return timeout;
	}

	/**
	 * @param timeout 要设置的 timeout。
	
	 */
	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}

	/**
	 * @return 返回 from。
	
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @param from 要设置的 from。
	
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * @return 返回 groupSize。
	
	 */
	public int getGroupSize() {
		return groupSize;
	}

	/**
	 * @param groupSize 要设置的 groupSize。
	
	 */
	public void setGroupSize(int groupSize) {
		this.groupSize = groupSize;
	}

	/**
	 * 功能: <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月17日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmailProperties [host=" + host + ", username=" + username + ", password=" + password + ", auth=" + auth
				+ ", timeout=" + timeout + ", from=" + from + ", groupSize=" + groupSize + "]";
	}

	
}
