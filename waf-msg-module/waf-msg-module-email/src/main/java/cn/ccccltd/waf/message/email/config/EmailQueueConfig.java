package cn.ccccltd.waf.message.email.config;

import java.util.Properties;

import javax.jms.ConnectionFactory;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import cn.ccccltd.waf.message.email.constant.EmailConstants;
import cn.ccccltd.waf.message.email.properties.EmailProperties;

/**
 * 创建日期:2017年11月14日
 * Title: 邮件配置信息
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Configuration
@EnableJms
public class EmailQueueConfig {
	
	@Autowired
	private EmailProperties emailProperties;
	@Autowired
	private ConnectionFactory connectionFactory;
	
	@Bean(name  = "emailQueue")
	public ActiveMQQueue emailQueue() {
		return new ActiveMQQueue(EmailConstants.EMAIL_QUEUE);
		
	}
	/**
	 * 功能: 邮件队列模板<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月14日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean(name = "emailJmsTemplate")
	public JmsTemplate emailJmsTemplate() {
		
		JmsTemplate jmsTemplate = new JmsTemplate();
		
		jmsTemplate.setConnectionFactory(connectionFactory);
		jmsTemplate.setDefaultDestinationName(EmailConstants.EMAIL_QUEUE);

		return jmsTemplate;
		
	}
	/**
	 * 功能: 邮件表更新状态队列<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月14日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean(name  = "emailUpdateTableQueue")
	public ActiveMQQueue emailUpdateTableQueue() {
		return new ActiveMQQueue(EmailConstants.EMAIL_UPDATETABLE_QUEUE);
		
	}
	
	/**
	 * 功能: 邮件表更新状态模板<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月14日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean(name = "emailUpdateTableJmsTemplate")
	public JmsTemplate emailUpdateTableJmsTemplate() {
		
		JmsTemplate jmsTemplate = new JmsTemplate();
		
		jmsTemplate.setConnectionFactory(connectionFactory);
		jmsTemplate.setDefaultDestinationName(EmailConstants.EMAIL_UPDATETABLE_QUEUE);
		
		return jmsTemplate;
		
	}
	
	@Bean(name = "mailSender")
	public JavaMailSenderImpl mailSender() {
		
		JavaMailSenderImpl jmsi = new JavaMailSenderImpl();
		jmsi.setHost(emailProperties.getHost());
		jmsi.setUsername(emailProperties.getUsername());
		jmsi.setPassword(emailProperties.getPassword());
		jmsi.setDefaultEncoding("UTF-8");
		Properties prop = new Properties();
		prop.setProperty("mail.smtp.auth", emailProperties.getAuth());
		prop.setProperty("mail.smtp.timeout", emailProperties.getTimeout());
		jmsi.setJavaMailProperties(prop);
		
		return jmsi;
		
	}
	
	@Bean(name = "simpleMailMessage")
	public SimpleMailMessage simpleMailMessage() {
		
		SimpleMailMessage smm = new SimpleMailMessage();
		smm.setFrom(emailProperties.getFrom());
		
		return smm;
		
	}
	
}
