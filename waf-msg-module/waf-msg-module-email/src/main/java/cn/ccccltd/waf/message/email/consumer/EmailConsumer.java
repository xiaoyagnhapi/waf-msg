package cn.ccccltd.waf.message.email.consumer;

import java.util.Map;

import javax.annotation.Resource;
import javax.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;

import cn.ccccltd.waf.message.base.consumer.BaseConsumer;
import cn.ccccltd.waf.message.email.constant.EmailConstants;
import cn.ccccltd.waf.message.init.PropertyContext;
import cn.ccccltd.waf.message.queue.log.util.LogMessageUtils;

/**
 * 创建日期:2017年11月14日
 * Title: 邮件处理的类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Component("emailConsumer")
public class EmailConsumer extends BaseConsumer{

	private static Logger log = LoggerFactory.getLogger(EmailConsumer.class);
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Resource(name="emailUpdateTableJmsTemplate")
	private JmsTemplate emailUpdateTableJmsTemplate;
	
	
	/**
	 * 功能: 邮箱的消费者 <br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月14日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param message
	 * @see cn.ccccltd.waf.message.base.consumer.BaseConsumer#receiveMessage(javax.jms.Message)
	 */
	@JmsListener(destination=EmailConstants.EMAIL_QUEUE, containerFactory="jmsListenerContainerFactory" )
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void receiveMessage(Message message) {
		
		super.receiveMessage(message);
		
	}
	

	@Override
	protected Map<String, String> sendMessage(JSONObject emailMessage) {
		
		String code = "200";
		String message = "邮件发送成功！";
		
		/**
		 * 具体的发送消息的
		 */
		final SimpleMailMessage smm = new SimpleMailMessage();
		String from = PropertyContext.getProperty("mail.default.from");
		smm.setFrom(from);
		smm.setTo(emailMessage.getString("emailAddress"));
		smm.setSubject(emailMessage.getString("msgTopic"));
		smm.setText(emailMessage.getString("content"));

		try {
			mailSender.send(smm);
		} catch (MailException e) {
			log.error("cn.ccccltd.waf.message.email.consumer.EmailConsumer.sendMessage(JSONObject)发送邮件失败！");
			code = "500";
			message = e.getLocalizedMessage();
			e.printStackTrace();
		}
		
		/**
		 * smsMessage.getString("emailId") 邮箱等真实表的id
		 */
		return LogMessageUtils.getSendLogMap(emailMessage.getString("emailId"), code, message);
	}

	@Override
	protected JmsTemplate getJmsTemplate() {
		
		return emailUpdateTableJmsTemplate;
		
	}
	
}
