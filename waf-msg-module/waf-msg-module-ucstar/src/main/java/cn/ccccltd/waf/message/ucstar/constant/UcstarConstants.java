package cn.ccccltd.waf.message.ucstar.constant;

/**
 * 创建日期:2017年11月2日
 * Title: 交建通常量定义
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class UcstarConstants {
	/**交建通队列定义*/
	public static final String UCSTAR_QUEUE = "waf.message.queue.ucstar";
}
