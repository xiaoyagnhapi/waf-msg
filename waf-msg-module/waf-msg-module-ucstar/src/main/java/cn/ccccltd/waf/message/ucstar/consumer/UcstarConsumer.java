package cn.ccccltd.waf.message.ucstar.consumer;

import javax.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import cn.ccccltd.waf.message.base.consumer.ConsumerService;
import cn.ccccltd.waf.message.ucstar.constant.UcstarConstants;

/**
 * 创建日期:2017年10月31日
 * Title:交建通的处理类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Component("ucstar")
public class UcstarConsumer implements ConsumerService{

	private static Logger log = LoggerFactory.getLogger(UcstarConsumer.class);
	
	/**
	 * 功能: 交建通的消费者<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年10月31日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @param msg
	 * @see cn.ccccltd.waf.message.base.consumer.ConsumerService#receiveMessage(java.lang.String)
	 */
	@JmsListener(destination=UcstarConstants.UCSTAR_QUEUE, containerFactory="jmsListenerContainerFactory" )
	@Override
	public void receiveMessage(Message msg) {
		
		
		log.info("接收的消息为======================"+msg);
		
	}

}
