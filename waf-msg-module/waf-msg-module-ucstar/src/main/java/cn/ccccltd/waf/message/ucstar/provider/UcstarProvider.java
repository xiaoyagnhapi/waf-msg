package cn.ccccltd.waf.message.ucstar.provider;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jms.core.JmsTemplate;

/**
 * 创建日期:2017年11月2日
 * Title:交建通的消费者
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
public class UcstarProvider{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Resource(name="ucstarJmsTemplate")
	private JmsTemplate jmsTemplate;
	

}
