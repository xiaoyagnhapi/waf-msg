package cn.ccccltd.waf.message.ucstar.config;

import javax.jms.ConnectionFactory;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;

import cn.ccccltd.waf.message.ucstar.constant.UcstarConstants;

/**
 * 创建日期:2017年11月2日
 * Title:交建通配置类
 * Description：对本文件的详细描述，原则上不能少于50字
 * @author yangjingjiang
 * @mender：（文件的修改者，文件创建者之外的人）
 * @version 1.0
 * Remark：认为有必要的其他信息
 */
@Configuration
@EnableJms
public class UcstarQueueConfig {
	
	@Autowired
	private ConnectionFactory connectionFactory;
	
	@Bean
	public ActiveMQQueue queue() {
		return new ActiveMQQueue(UcstarConstants.UCSTAR_QUEUE);
		
	}
	
	/**
	 * 功能: 交建通队列模板<br>
	 * 作者: yangjingjiang <br>
	 * 创建日期:2017年11月2日 <br>
	 * 修改者: mender <br>
	 * 修改日期: modifydate <br>
	 * @return
	 */
	@Bean
	public JmsTemplate ucstarJmsTemplate() {
		
		JmsTemplate jmsTemplate = new JmsTemplate();
		
		jmsTemplate.setConnectionFactory(connectionFactory);
		jmsTemplate.setDefaultDestinationName(UcstarConstants.UCSTAR_QUEUE);
		
		return jmsTemplate;
		
	}
}
